public class UpdateAccountController {
	
    @AuraEnabled
    public static List<Contact> returnContact(Id accId){
        List<Contact> lstCon  = [select Id,LastName,FirstName,OwnerId,AccountId, Email ,Owner.Name, CreatedDate from Contact where AccountId =:accId limit 5];
        
        return lstCon;
    }
    @AuraEnabled
    public static string updateContact(String fieldsToBeUpdated){
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(fieldsToBeUpdated);
        
        Contact con = new contact(Id=String.valueof(m.get('conId')));
        con.LastName = String.valueof(m.get('LastName'));
        con.FirstName = String.valueof(m.get('FirstName'));
        try{
            update con;
        }catch(Exception ee){
            
        }
        system.debug('fieldsToBeUpdated :: '+fieldsToBeUpdated);
        return fieldsToBeUpdated;
    }
}