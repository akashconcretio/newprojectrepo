global class executeInPB{
    @InvocableMethod(label='Test')
    global static List<String> calledByProcessBuilder(List<innerClass> ids){
        if(ids!=null && ids[0].accId!=null){
            Id accId = ids[0].accId;
            Account acc = [select id,CustomerPriority__c,ShippingCity,ShippingState,ShippingCountry,ShippingPostalCode,ShippingStreet from Account where Id=:accId limit 1];
            // acc.CustomerPriority__c = 'High';
            // update acc;
            List<Contact> lstCon = [select MailingStreet,MailingCity,MailingPostalCode,MailingCountry,MailingState from contact where AccountId=:accId];
            for(Contact con : lstCon){
                con.MailingStreet=acc.ShippingStreet;
                con.MailingCity=acc.ShippingCity;
                con.MailingPostalCode=acc.ShippingPostalCode;
                con.MailingCountry=acc.ShippingCountry;
                con.MailingState=acc.ShippingState;
            }
            update lstCon;
        }
        return null;
    }
    global class innerClass{
        @InvocableVariable
        global String accId;
    }
}