public class MaintenanceRequestHelper {
    
    public static void updateWorkOrders(List<Case> lstCases){
        // update workorders
        list<case> lstcse = new list<case>();
        for(case cse : lstCases){
            if((cse.Type.equalsignorecase('Routine Maintenance') || cse.Type.equalsignorecase('Routine')) && cse.status.equalsignorecase('Closed')){
                lstcse.add(new case(Product__c=cse.Product__c,type='Routine Maintenance',status='New',Date_Reported__c=cse.createdDate.Date(),subject='test',Vehicle__c=cse.Vehicle__c,Date_Due__c=cse.createdDate.Date().addDays(30),Equipment__c=cse.Equipment__c));
            }
        }
        if(lstcse.size()>0){
            insert lstcse;
        }
        
    }        
    
}