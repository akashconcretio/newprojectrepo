public class ObjectSpecificFilter{
    public List<String> lstObj = new List<String>{'Account','Contact'};
    public ObjectSpecificFilter(){
        
    }
    public class objectDescription{
        public string obj;
        public List<fieldDescription> fieldDesc;
        public objectDescription(string obj,List<fieldDescription> fieldDesc){
            this.obj = obj;
            this.fieldDesc = fieldDesc;
        }
        
    }
    public class fieldDescription{   
        public string fieldApiName;
        public string fieldType;
        public List<opretor> fieldSpecificOperator;
        public string fieldLabel;
        public fieldDescription(String fieldApiName,string fieldType,List<opretor> fieldSpecificOperator,String fieldLabel){
            this.fieldType = fieldType;
            this.fieldSpecificOperator = fieldSpecificOperator;
            this.fieldApiName = fieldApiName;
            this.fieldLabel = fieldLabel;
        }
    }
    public class opretor{
        public string operator;
        public opretor(string operator){
            this.operator = operator;
        }
    }
    @RemoteAction
    public static string allAboutObjectSpecificFilter(){
        List<String> lstObj = new List<String>{'Account','Contact','Opportunity'};
        // map<String,Map<String,fieldDescription>> mapOfObjectFieldOperator = new map<String,Map<String,fieldDescription>> ();
        List<map<String,Map<String,fieldDescription>>> lst = new List<map<String,Map<String,fieldDescription>>>() ;
        List<objectDescription> lstOfobjectDescription = new List<objectDescription>();
        for(string str : lstObj){
            // map<String,Map<String,fieldDescription>> mapOfObjectFieldOperator = new map<String,Map<String,fieldDescription>> ();
            // setFieldOperator(str,lstOfobjectDescription);
            // lst.add(mapOfObjectFieldOperator);
            
            lstOfobjectDescription.add(new objectDescription(str,setFieldOperator(str,new List<fieldDescription>())));
        }
        system.debug('mapOfObjectFieldOperator :: '+'{"objName":'+JSON.serialize(lstOfobjectDescription)+'}');
        return '{"objName":'+JSON.serialize(lstOfobjectDescription)+'}';
    }
    
    public static List<fieldDescription> setFieldOperator(string obj,List<fieldDescription> lstOfFieldDescription){
        SObjectType sobjType = Schema.getGlobalDescribe().get(obj);
        Map<String,Schema.SObjectField> mfields = sobjType.getDescribe().fields.getMap();
        // mapOfObjectFieldOperator.put(obj,new Map<String,fieldDescription>());
        list<opretor> lstOPerator = new list<opretor>();
        lstOPerator.add(new opretor('!='));
        lstOPerator.add(new opretor('='));
        
        for(string str : mfields.keySet()){
            string fType = String.valueof(mfields.get(str).getDescribe().getType());
            string fApiName = String.valueof(mfields.get(str).getDescribe().getName());
            string fLabel = String.valueof(mfields.get(str).getDescribe().getLabel());
            
            lstOfFieldDescription.add(new fieldDescription(fApiName,fType,lstOPerator,fLabel));
            // mapOfObjectFieldOperator.get(obj).put(fApiName,new fieldDescription(fType,new List<String>{'!=','='}));
        }
        
        return lstOfFieldDescription;
    }
}