public class Account_Svc {
	
    public interface sacnner{
        Boolean scan(Account acc);
    }
    
    public static Account_Svc self{
        get{
            if(self == null){
                return new Account_Svc();
            }
            return self;
        }set;    
    }
    
    public Map<String,sacnner> mapOfHandler = new Map<String,sacnner>{
        'Rating' => new AccountRating(),
        'Industry' => new AccountIndustry(),
        'Type' => new AccountType()
    };
    
    public class AccountIndustry implements sacnner{
        public Boolean scan(Account acc){
            return String.isBlank(acc.Industry)?true:false;
        }
    }
    public class AccountRating implements sacnner{
        public Boolean scan(Account acc){
            return String.isBlank(acc.Rating)?true:false;
        }
    }
    public class AccountType implements sacnner{
        public Boolean scan(Account acc){
            return String.isBlank(acc.Type)?true:false;
        }
    }
    
    public static Map<Id,Set<String>> returnReasons(List<Account> lstAcc){
        Map<Id,Set<String>> mapReasons = new Map<Id,Set<String>>();
        
        for(Account acc : lstAcc){
            for(String str : Account_Svc.self.mapOfHandler.keyset()){
                Account_Svc.sacnner scanner = Account_Svc.self.mapOfHandler.get(str);
                if(scanner.scan(acc)){
                    if(mapReasons.containsKey(acc.Id))
                        mapReasons.get(acc.Id).add(str);
                    else
                        mapReasons.put(acc.Id,new Set<String>{str});
                }
            }
        }
        return mapReasons;
    }
}