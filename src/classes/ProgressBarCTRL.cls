public class ProgressBarCTRL {
    @AuraEnabled
    public static String setProgressBar(Id accId,String Status){
        List<Contact> con = [select id,Progress_Status__c  from contact where AccountId =:accId];
            
        system.debug('Status :: '+Status);
        system.debug('accId :: '+accId);
        
        if(Status!=''){
            con[0].Progress_Status__c = Status;
            update con; 
            return con[0].Progress_Status__c;
        }else if(con.size()>0){
            return con[0].Progress_Status__c==null?'Requested':con[0].Progress_Status__c;
        }else{
            return 'Requested';
        }
    }
}