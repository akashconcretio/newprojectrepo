public class wrapperClassController {
      public String selectedAccId{get;set;}
    public List<cContact> contactList {get; set;}
    
    public List<cContact> getContacts() {
        
            contactList = new List<cContact>();
              
            for(Contact c : [select Id, Name, Email, Phone from Contact ])
            {
                contactList.add(new cContact(c));
            }
     


        return contactList;
    }
    public class cContact {
        public Contact con {get; set;}
        public Boolean selected {get; set;}
        public cContact(Contact c)
        {
            con = c;
            selected = false;
        }
    }
}