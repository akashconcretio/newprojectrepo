public class BillDescription {
       Public BillDetails newBillDetails{get;set;}
       Public BillDescription(){
           newBillDetails = BillDetails.parse('{"id": "2938293","businessName": "Bill\'s Auto Shop","contactFirstName": "Bill","contactLastName": "Smith","contactEmail": "bill@billsauto.com","contactPhone": "2123219398","locations": [{"id": "39483","locationName": "Bill\'s Auto Shop","address": "144 Jones St","city": "Springfield","state": "VA", "zip": "22104","phone": "7032348344","categoryIds": ["1"]}],"subscriptions": [{"offerId": 2687,"locationIds": ["39483"]}]}');
       }
}