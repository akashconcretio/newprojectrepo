public class TriggerActionController { 
    public static void setTriggerActionAndRelatedHandlers(List<Sobject> newList,List<Sobject> oldList){
        if(RunOnce.RunOnce){
            RunOnce.RunOnce();
            Map<String,List<Trigger_Handler__c>> mapOFHandlersPerAction = new Map<String,List<Trigger_Handler__c>>{
                'After Insert' => new List<Trigger_Handler__c>(),
                    'Before Insert' => new List<Trigger_Handler__c>(),
                    'After Update' => new List<Trigger_Handler__c>(),
                    'Before Update' => new List<Trigger_Handler__c>()
                    };
                        
            List<Trigger_Handler__c> lstTH = [select Handler_Name__c,Active__c,Object__c,Order_Of_Execution__c,Trigger_Actions__c, Execute_Asynchronous__c  from Trigger_Handler__c where Object__c = 'Contact' ORDER BY Order_Of_Execution__c ASC];
            if(lstTH.size() > 0){
                for(Trigger_Handler__c th : lstTH){
                    if(th.Active__c  && th.Trigger_Actions__c != null){
                        mapOFHandlersPerAction.get(th.Trigger_Actions__c).add(th);
                    }
                }
            }
            RunOnce.mapOFHandlersPerAction = mapOFHandlersPerAction;
            
        }
        //AsynchromousProcessController.proceedAsynchronously
        if(trigger.isInsert && trigger.isBefore && RunOnce.mapOFHandlersPerAction.get('Before Insert').size()>0){
            List<Trigger_Handler__c> lstHandlers = RunOnce.mapOFHandlersPerAction.get('Before Insert');
            for(Trigger_Handler__c handlerName : lstHandlers){
                TDTMInterface allInOneInterface = CreateHandler.returnHandler(handlerName.Handler_Name__c);
                if(allInOneInterface != null && !handlerName.Execute_Asynchronous__c){
                    allInOneInterface.onBeforeInsert(newList,oldList);
                }else if(allInOneInterface != null && handlerName.Execute_Asynchronous__c){
                    RunOnce.newList = trigger.new;
                    RunOnce.oldList = trigger.old;
                    AsynchromousProcessController.proceedAsynchronously(handlerName.Handler_Name__c,'BeforeInsert');
                }
            }
        }else if(trigger.isInsert && trigger.isAfter && RunOnce.mapOFHandlersPerAction.get('After Insert').size()>0){
            List<Trigger_Handler__c> lstHandlers = RunOnce.mapOFHandlersPerAction.get('After Insert');
            for(Trigger_Handler__c handlerName : lstHandlers){  
                TDTMInterface allInOneInterface = CreateHandler.returnHandler(handlerName.Handler_Name__c);
                if(allInOneInterface != null && !handlerName.Execute_Asynchronous__c){
                    allInOneInterface.onAfterInsert(newList,oldList);
                }else if(allInOneInterface != null && handlerName.Execute_Asynchronous__c){
                    RunOnce.newList = trigger.new;
                    RunOnce.oldList = trigger.old;
                    AsynchromousProcessController.proceedAsynchronously(handlerName.Handler_Name__c,'AfterInsert');
                }
            }
        }else if(trigger.isUpdate && trigger.isBefore && RunOnce.mapOFHandlersPerAction.get('Before Update').size()>0){
            List<Trigger_Handler__c> lstHandlers = RunOnce.mapOFHandlersPerAction.get('Before Update');
            for(Trigger_Handler__c handlerName : lstHandlers){
                TDTMInterface allInOneInterface = CreateHandler.returnHandler(handlerName.Handler_Name__c);
                if(allInOneInterface != null && !handlerName.Execute_Asynchronous__c){
                    allInOneInterface.onBeforeUpdate(newList,oldList);
                }else if(allInOneInterface != null && handlerName.Execute_Asynchronous__c){
                    RunOnce.newList = trigger.new;
                    RunOnce.oldList = trigger.old;
                    AsynchromousProcessController.proceedAsynchronously(handlerName.Handler_Name__c,'BeforeUpdate');
                }
            }
        }else if(trigger.isUpdate && trigger.isAfter && RunOnce.mapOFHandlersPerAction.get('After Update').size()>0){
            List<Trigger_Handler__c> lstHandlers = RunOnce.mapOFHandlersPerAction.get('After Update');
            for(Trigger_Handler__c handlerName : lstHandlers){
                TDTMInterface allInOneInterface = CreateHandler.returnHandler(handlerName.Handler_Name__c);
                if(allInOneInterface != null && !handlerName.Execute_Asynchronous__c){
                    allInOneInterface.onAfterUpdate(newList,oldList);
                }else if(allInOneInterface != null && handlerName.Execute_Asynchronous__c){
                    RunOnce.newList = trigger.new;
                    RunOnce.oldList = trigger.old;
                    AsynchromousProcessController.proceedAsynchronously(handlerName.Handler_Name__c,'AfterUpdate');
                }
            }
        }
    }
}