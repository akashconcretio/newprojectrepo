public class AccountListSLDS {
    public list<Account> lstAcc {get;set;}
    public string selected{get;set;}
    public list<selectOption> accIndustry{get;set;}
    public AccountListSLDS(){
        selected = 'Prospect';
        accIndustry = new list<selectOption>();
        accIndustry = getAllIndustryType();
        getAccList();
    }
    public void getAccList(){
        string query;
        lstAcc = new list<Account>();
        query = 'select Name,Industry,Phone,website,Rating from Account where Type =:selected limit 10';
        system.debug('query::'+query);
        lstAcc =  database.query(query);
        system.debug('lstAcc::'+lstAcc);
    }
    public List<SelectOption> getAllIndustryType(){
        List<SelectOption> options = new List<SelectOption>();
        
           Schema.DescribeFieldResult fieldResult =
         Account.Type.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
           }       
           return options;
    }
}