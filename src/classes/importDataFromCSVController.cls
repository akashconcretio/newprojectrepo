public class importDataFromCSVController {
    
    public String documentName {get;set;}
    public Blob csvFileBody{get;set;}
    String contactDataAsString;
    List<string> csvRows ; 
    public importDataFromCSVController(){
        
    }
    
    
    public  void readFromFile(){
        try{
            csvRows = new List<string>();
            contactDataAsString = csvFileBody.toString();
            csvRows.addAll(contactDataAsString.split('\n'));
            system.debug('csvRows ::' +csvRows);
            //readCSVFile();
        }
        catch(exception e){
            //readSuccess = FALSE;
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,'Error reading CSV file');
            ApexPages.addMessage(errorMessage);
        }
    }    
}