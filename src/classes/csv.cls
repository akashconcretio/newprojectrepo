public class csv 
{
    public Account acc{get;set;}
    public boolean showhide{get;set;}
    public string selected{get;set;}
    public list<selectoption> lstacc{get;set;}
public csv()
{
  acc=new Account();
  showhide=false;
   lstacc=new list<selectoption>();
   lstacc.add(new selectoption('','none'));
   lstacc.add(new selectoption('pdf','pdf'));
   lstacc.add(new selectoption('msword','msword'));
   lstacc.add(new selectoption('excel','excel'));
}
 public void save()
 {
   showhide=true;
   
 }
  public pagereference download()
  { 
    if(selected =='pdf')
    {
      pagereference pagref=new pagereference('/apex/page1');
        pagref.setredirect(true);
     return pagref;
    }
   if(selected =='msword')
    {
     pagereference pagref1=new pagereference('/apex/page2');
        pagref1.setredirect(true);
     return pagref1;
    }
    return null;
  }
  public pagereference gopage1()
  {  pagereference pagref=new pagereference('/apex/page1');
        pagref.setredirect(true);
     return pagref;
  }
  public pagereference gopage2()
  {
     pagereference pagref1=new pagereference('/apex/page2');
        pagref1.setredirect(true);
     return pagref1;
  }
  
  
  

}