public class BillDetails{
	public String id;	//2938293
	public String businessName;	//Bill's Auto Shop
	public String contactFirstName{get;set;}	//Bill
	public String contactLastName;	//Smith
	public String contactEmail{get;set;}	//bill@billsauto.com
	public String contactPhone;	//2123219398
 	public cls_locations[] locations{get;set;}
 	public cls_subscriptions[] subscriptions{get;set;}
	Public class cls_locations {
		public String id{get;set;}	//39483
		public String locationName;	//Bill's Auto Shop
		public String address;	//144 Jones St
		public String city;	//Springfield
		public String state{get;set;}	//VA
		public String zip;	//22104
		public String phone;	//7032348344
	//	public cls_categoryIds[] categoryIds{get;set;}
	}
// 	public class cls_categoryIds {
// 		public String categoryIds;	//1
// 	}
	public class cls_subscriptions {
		public Integer offerId{get;set;}	//2687
		//public cls_locationIds[] locationIds;
	}
 //	public class cls_locationIds {
//		public String locationId;	//3
// 		public String 1;	//9
// 		public String 2;	//4
// 		public String 3;	//8
// 		public String 4;	//3
//	}
	public static BillDetails parse(String json){
		return (BillDetails) System.JSON.deserialize(json, BillDetails.class);
	}


}