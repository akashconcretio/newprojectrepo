@isTest
private class TestAutomaticLogic {

	private static testMethod void TestAutomaticLogic() {
	        Product2 p2 = new Product2();
            p2.Name = 'testProduct';
            p2.IsActive = true;
            p2.Cost__c = 1223;
            p2.Current_Inventory__c = 5;
            p2.Lifespan_Months__c = 23;
            p2.Maintenance_Cycle__c = 30;
            p2.Replacement_Part__c = true;
            p2.Warehouse_SKU__c = '987655353';
            insert p2;
            
            Vehicle__c veh = new Vehicle__c();
            veh.Name = 'vehicle';
            insert veh;
	    
	    
	    
	    Case cse =new case();
	    cse.status = 'New';
	    cse.Equipment__c = p2.Id;
	    cse.Vehicle__c = veh.id;
	    cse.Type = 'Routine Maintenance';
	    
	    insert cse;
	    
	    case cs = [select id,status,Type from case limit 1];
	    cs.status = 'Closed';
	    cs.Reason = 'Performance';
	    
	    update cs;
	}

}