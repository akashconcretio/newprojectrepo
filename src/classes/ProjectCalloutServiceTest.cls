@isTest
private class ProjectCalloutServiceTest {
    @testSetup static void setUp() {
        ServiceTokens__c token = new ServiceTokens__c();
        token.name = 'ProjectServiceToken';
        token.Token__c = '56062cd6-ea5b-4f92-bb68-852f0fde06a0';
        insert token;
    }
    
	private static testMethod void test() {
	    Test.startTest();
	        Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMock());
	    
	        Opportunity opp = new Opportunity();
	        opp.Name = 'Test';
	        opp.Type = 'New Project';
	        opp.stageName = 'closed won';
	        opp.closeDate = Date.newInstance(2018, 05, 23);
	        insert opp;
	        
	    Test.stopTest();
	}

}