public class firstComponentCtrl {
    public firstComponentCtrl(){}
    @AuraEnabled
    public static allData getContacts(string d) {
        List<string> lstOfApiName = new List<string>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('Contact');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('conf');
        for(Schema.FieldSetMember fs:fieldSetObj.getFields()){
            lstOfApiName.add(fs.getFieldPath());
        }
        integer val = Integer.valueof(d);
        list<checkBox> lsCb = new list<checkBox>();
        string query = 'select '+string.join(lstOfApiName,',')+' From Contact where EMAIL = null limit :val';
        List<contact> lstCon = DataBase.query(query);
        for(contact cn : lstcon){
            lsCb.add(new checkBox(cn,false));
        }
        allData stAllData = new allData(lsCb,getFields());
        system.debug('stAllData::'+stAllData);
        return stAllData;
    }
	@AuraEnabled
    public static allData getIndurtsy(string conId,String limitval) {
        integer setlimit = integer.valueof(limitval);
        list<checkBox> lsCb = new list<checkBox>();
        string query = 'select Id, Name, Email, Title, Phone From Contact where '+conId+' Limit '+setlimit;
        List<contact> lstCon = DataBase.query(query);
        for(contact cn : lstcon){
            lsCb.add(new checkBox(cn,false));
        }
        allData stAllData = new allData(lsCb,getFields());
        return stAllData;
    }
    @AuraEnabled
    public static List<PickListWrapper> getPickListFields(String fieldApi) {
        List<PickListWrapper> str = new List<PickListWrapper>();
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();
        if(fieldMap!=null && fieldMap.get(fieldApi).getDescribe().isAccessible()){
            List<Schema.PicklistEntry> PicklistValues = fieldMap.get(fieldApi).getDescribe().getPicklistValues();
            for(Schema.PicklistEntry pickListValue : PicklistValues) {
                str.add(new PickListWrapper(string.valueof(pickListValue.getLabel()), string.valueof(pickListValue.getValue())));
            }
        }
        return str;
    }
    @AuraEnabled
    public static List<PickListWrapper> getOtherFields(String fieldApi) {
        List<PickListWrapper> str = new List<PickListWrapper>();
        str.add(new PickListWrapper('True','True'));
        str.add(new PickListWrapper('False','False'));
        return str;
    }
    public class PickListWrapper{
        @AuraEnabled
        public String Label{get;set;}
        @AuraEnabled
        public String Value{get;set;}
        public PickListWrapper(String Label1, String Value1){
            Label = Label1;
            Value = Value1;
        }
    } 
    @AuraEnabled
    public static List<fieldsApiLabel> getFields() {
        List<fieldsApiLabel> fields = new List<fieldsApiLabel>();
        Map<String,Schema.SObjectField> mfields = Contact.sobjectType.getDescribe().fields.getMap();
        for(String str :mfields.KeySet()){
            Schema.DescribeFieldResult fr = mfields.get(str).getDescribe();
            fields.add(new fieldsApiLabel(string.valueof(fr.getLabel()),str,string.valueof(fr.getType())));
        }
        return fields;
    }
    @AuraEnabled
    public static string getSelected(string lstCheckBox) {
        string str = lstCheckBox.removeEnd(',');
        List<string> lstOfStr = str.split(',');
        List<contact> lstContact = [select id from contact where id IN: lstOfStr];
        delete lstContact;
        return  'ok';
    }
    public class checkBox{
        @AuraEnabled
        public contact con{get;set;}
        @AuraEnabled
        public Boolean selected{get;set;}
        public checkBox(Contact ct,Boolean bl){
            con = ct;
            selected = bl;
        }
    }
    public class fieldsApiLabel{
        @AuraEnabled
        public String Label{get;set;}
        @AuraEnabled
        public String api{get;set;}
        @AuraEnabled
        public String fieldType{get;set;}
        public fieldsApiLabel(String lbl,String ap,string dType){
            Label = lbl;
            api  = ap; 
            fieldType = dType;
        }
    }
    public class allData{
        @AuraEnabled
        public List<checkBox> lstCheckBox{get;set;}
        @AuraEnabled
        public List<fieldsApiLabel> lstFieldsApiLabel{get;set;}
        public allData(List<checkBox> lc,List<fieldsApiLabel> lf){
            lstCheckBox = lc;
            lstFieldsApiLabel = lf;
        }
    }
}