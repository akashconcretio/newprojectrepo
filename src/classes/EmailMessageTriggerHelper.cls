public class EmailMessageTriggerHelper { 

    public static void publishPlatFromEventForCaseEmailMessage(List<EmailMessage> newEmailMessage){
        user usr = [select id, Show_Email_Message_Notification__c  from user where Id=:userInfo.getUserId()];
        if(!usr.Show_Email_Message_Notification__c && !Test.isRunningTest()){
            return;
        }
        
        List<EmailMessageNotification__e> lstEmailMessageNotification = new List<EmailMessageNotification__e>();
        Map<Id,Id> mapEmailMessageAndCaseIds = new Map<Id,Id>();
        for(EmailMessage em : newEmailMessage){
            mapEmailMessageAndCaseIds.put(em.Id,em.parentId);
        }
        
        Map<Id,case> mapCase = new Map<Id,case>([select Id,CaseNumber,Account.Name,Subject,Priority  from case where Id IN:mapEmailMessageAndCaseIds.values()]);
        
        for(Id  emailMessageId : mapEmailMessageAndCaseIds.keySet()){
            if(mapCase.containsKey(mapEmailMessageAndCaseIds.get(emailMessageId))){
                Case cse = mapCase.get(mapEmailMessageAndCaseIds.get(emailMessageId));
                
                EmailMessageNotification__e emn = new EmailMessageNotification__e();
                emn.value__c = '{"CaseNumber":"'+cse.CaseNumber+'","AccountName":"'+cse.Account.Name+'","Subject":"'+cse.Subject+'","Priority":"'+cse.Priority+'","CaseId":"'+cse.Id+'"}';
                
                lstEmailMessageNotification.add(emn);
            }
        }
        
        // Call method to publish events
        List<Database.SaveResult> results = EventBus.publish(lstEmailMessageNotification);
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' +err.getStatusCode() +' - ' +err.getMessage());
                }
            }       
        }
    }
}