@RestResource(urlMapping='/Accounts/*')
global with sharing class AccountManager {
    
    @HttpGet
    global static Account getAccount() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String accId = request.requestURI.substring(request.requestURI.length()-27,request.requestURI.length()-9);
          
        Account result =  [SELECT Id,Name,(select Id,Name from contacts) FROM Account WHERE Id = :accId];
        return result;
    }
    // public class accDetails{
    //     public Id accId;
    //     public String accName;
    //     public List<Contact> lstContacts;
    //     public accDetails(Id acc,String nm,List<Contact> listContacts){
    //         accId = acc;
    //         accName=nm;
    //         lstContacts = listContacts;
    //     }
    // }
    
}