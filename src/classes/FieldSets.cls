public class FieldSets {
    public Account merch { get; set; }
    
    public FieldSets() {
        merch = getMerchandise();
    }

    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Account.FieldSets.test.getFields();
    }

    private Account getMerchandise() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'type FROM Account LIMIT 1';
        return Database.query(query);
    }
}