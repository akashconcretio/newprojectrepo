public class CreateHandler {
    public static TDTMInterface returnHandler(String className){
        Type t = Type.forName(className);
        Object obj = t.newInstance();
        if(obj instanceof TDTMInterface){
            TDTMInterface tdtmINt = (TDTMInterface) obj; 
        	return tdtmINt;
        }
        return null;
    }
}