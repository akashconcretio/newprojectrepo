public class ContactHandler implements TDTMInterface{
    
    Public void onBeforeInsert(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        system.debug('ContactHandler :: '+'onBeforeInsert');
    }
    
    Public void onAfterInsert(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        system.debug('ContactHandler :: '+'onAfterInsert');
    }
    
    Public void onBeforeUpdate(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        system.debug('ContactHandler :: '+'onBeforeUpdate');
    }
    
    Public void onAfterUpdate(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        system.debug('ContactHandler :: '+'onAfterUpdate');
    }
    Public void onAfterDelete(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        
    }
}