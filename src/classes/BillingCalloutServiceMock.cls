@isTest
global class BillingCalloutServiceMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        String expectedResult = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">' 
            + '<Body><createLead xmlns="http://www.mnet.de/esb/cdm/Sales/LeadService/v1"><residentialLead><title>TestTitle</title><firstName>TestFirstName</firstName><middleName></middleName><lastName>TestLastName</lastName><communicationLanguage>German</communicationLanguage><disposition>Qualified</disposition><distributionChannel>E-Mail</distributionChannel><address><locationId>12345678</locationId></address><campaign><id></id><retention>false</retention></campaign><email optIn="true">test@mailinator.com</email></residentialLead></createLead></Body>'
            + '</Envelope>';
            
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody(expectedResult);
        res.setStatusCode(200);
        return res;
    }
}