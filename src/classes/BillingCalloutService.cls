global class BillingCalloutService {
    //Implement business and callout logic methods here
    @future(callout=true)
    global static void callBillingService(String lstOfProject){
        for(Project__c prj : (List<Project__c>)JSON.deserialize(lstOfProject,List<Project__c>.Class)){
             if(prj.Status__c == 'Billable'){
                    // system.debug('sta : '+prj.Status__c);
                    BillingServiceProxy.project prjt = new BillingServiceProxy.project();
                    prjt.username = 'bsUser1';
                    prjt.password = 'bsPASS1';
                    prjt.projectRef = prj.ProjectRef__c;
                    prjt.billAmount = prj.Billable_Amount__c;
                    
                    BillingServiceProxy.InvoicesPortSoap11 inv = new BillingServiceProxy.InvoicesPortSoap11();
                    string str = inv.billProject(prjt);
                    system.debug('sdf : '+str);
                    
             }
        }
       
    }
}