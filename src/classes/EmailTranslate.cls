public class EmailTranslate {
    public list<selectoption> lstOpt {get;set;}
    public string selected{get;set;}
    public string generatedEmailBody{get;set;}
    public map<string,sobject> mapCS = new map<string,sobject>();
    public string templateID{get;set;}
    public map<string,string> mapTempIdLangCombination = new map<string,string>();
    public EmailTranslate(){
        mapCS = EmailTranslateCaseCreation__c.getAll();
        for(string str : mapCS.keyset()){
            mapTempIdLangCombination.put(str,string.valueof(mapCS.get(str).get('Template_ID__c')));
        }
        templateID = apexpages.currentpage().getparameters().get('tempid'); 
        selected = templateID;
        lstOpt = new list<selectoption>();
        for(string strr : mapTempIdLangCombination.keyset()){
            lstOpt.add(new selectoption(string.valueof(mapCS.get(strr).get('Template_ID__c')),strr));
        }
    }
    public void getEmailBody(){
        contact con = new contact();
        con.lastname = 'Akash';
        con.Email = 'akash@concret.io';
        Insert con;
        Messaging.SingleEmailMessage[] previewEmails = new Messaging.SingleEmailMessage[]{};
        Messaging.SingleEmailMessage firstPreviewEmail = new Messaging.SingleEmailMessage();
        firstPreviewEmail.setUseSignature(false);
        firstPreviewEmail.setSaveAsActivity(false);
        firstPreviewEmail.setTemplateId(selected);
        firstPreviewEmail.setTargetObjectId(con.id);
        previewEmails.add(firstPreviewEmail);
        Messaging.SingleEmailMessage secondPreviewEmail = new Messaging.SingleEmailMessage();
        previewEmails.add(secondPreviewEmail);
        try {
            List<Messaging.SendEmailResult> previewResult = Messaging.sendEmail(previewEmails);
        }
        catch (Exception e) {
            generatedEmailBody = firstPreviewEmail.getHtmlBody()+'</body>';  // Email body generated
            list<string> str = generatedEmailBody.split('<br>');
            generatedEmailBody = string.join(str,'');
        }
    }
}