public class ExceptionCheckingClassException extends Exception {
    public static void mainProcessing() {
        try {
            Account acc = new Account();
            acc.Name = 'test';
            insert acc;
        } catch(Exception me) {
            throw new ExceptionCheckingClassException(
                'It seems required field exists');    
        }
    }
}