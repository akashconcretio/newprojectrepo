public class LinkedIn_Ctrl {
    public String code{get;set;}
    
    public String accessTok{get;set;}
    
    public LinkedIn_Ctrl(){
        code = ApexPages.currentPage().getParameters().get('code');
        system.debug('code : '+code);
    }
    public PageReference  process(){
        String state = 'Akash123';
        String endPoint = 'https://www.linkedin.com/oauth/v2/authorization';
        
        String redirectPage = 'https://my1stltngpage-dev-ed--c.ap6.visual.force.com/apex/LinkedIn';
        String scope = 'r_liteprofile r_emailaddress';
        redirectPage = EncodingUtil.urlEncode(redirectPage, 'UTF-8');
        scope = EncodingUtil.urlEncode(scope, 'UTF-8');
        state = EncodingUtil.urlEncode(state, 'UTF-8');
        
        endPoint+='?response_type=code';
        endPoint+='&client_id='+'81dz8a8z24nglw';
        endPoint+='&redirect_uri='+redirectPage;
        endPoint+='&state='+state;
        endPoint+='&scope='+scope;
        
        return new PageReference(endPoint).setRedirect(true);
        
    }
    public void accessToken(){
        //access_token
        String redirectPage = 'https://my1stltngpage-dev-ed--c.ap6.visual.force.com/apex/LinkedIn';
        redirectPage = EncodingUtil.urlEncode(redirectPage, 'UTF-8');
        
        String endPoint = 'https://www.linkedin.com/oauth/v2/accessToken';
        endPoint+= '?grant_type=authorization_code';
        endPoint+= '&code='+code;
        endPoint+= '&redirect_uri='+redirectPage;
        endPoint+= '&client_id=81dz8a8z24nglw';
        endPoint+= '&client_secret=qR0AMMSWqJm3fmid';
        
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('Content-Length', '0');
		HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            accessTok = String.valueOf(results.get('access_token'));
            system.debug('accessTok :: '+accessTok);
        }
        
    }
    public void retrieveInfo(){
        //627b48122 SfYK6Ud3L0
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        // clientAwareMemberHandles?q=members&projection=(elements*(primary,type,handle~))'
        request.setEndpoint('https://api.linkedin.com/v2/me');
        request.setMethod('GET');  
        system.debug('accessTok Passed:: '+accessTok);
        String authorizationHeader = 'Bearer ' + accessTok;
        request.setHeader('Authorization',authorizationHeader);
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded'); 
        request.setHeader('Content-Length', '0');
		HttpResponse response = http.send(request);
        system.debug('body :: '+response.getBody());
    }
    
}