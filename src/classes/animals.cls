public class animals{
	public cls_animal animal;
	public class cls_animal {
		public Integer id;	//99
		public String name;	//trailhead
		public String eats;	//burritos
		public String says;	//more badgers
	}
	public static animals parse(String json){
		return (animals) System.JSON.deserialize(json, animals.class);
	}
}