global class Class_Name implements Database.Batchable<sobject>, Database.Stateful{
    Integer i = 0;
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('CameS');
        return Database.getQueryLocator('SELECT Id  FROM Contact limit 10');
    }
    
    global void execute(Database.BatchableContext bc, List<sobject> listEmployee){
        system.debug('Came');
        for(sobject e : listEmployee){
            //e.Sequence_Number__c = i;
            i += 1;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        system.debug(LoggingLevel.INFO,'i : '+i);
    }
}