/**
* @author Badan Singh Pundeer
* @date 20.05.2018
*
* @group CBM
*
* @description Apex class to set filter Fields.
*              And set Columns for Spender selection Export Table.
*
* @history
* version                           | author                                    | changes
*=============================================================================
* 1.0 (20.05.2018)          | Badan Pundeer                   | initial version
*/
public class CCG_SpenderSelectionConfiguration {
    public List<CCG_ColumnsDetailPrePopulationService> lstcolumnsDetail{get;set;}
    public String columnsDetail{get;set;}
    public String columnDataForObjects{get;set;}
    /**
    * @description Inside Constructor , We initializing columns related data which need to pre-populate While Page Loads.
    *              Like already set Columns.                     
    */
    public CCG_SpenderSelectionConfiguration(){
        lstcolumnsDetail = new List<CCG_ColumnsDetailPrePopulationService>();
        for(Spender_Selektion_Columns_Setting__c colSett : Spender_Selektion_Columns_Setting__c.getAll().values()){
            lstcolumnsDetail.add(new CCG_ColumnsDetailPrePopulationService(colSett.Name,colSett.Account_Field__c,colSett.Contact_Field__c,colSett.Fixed__c,colSett.Lead_Fields__c));
        }
        columnsDetail = JSON.serialize(lstcolumnsDetail);
        getAccountContactFieldsForColumns();
    }
    
    /**
    * @description constructing data for Columns Tab and called inside the constructor.
    */
    public void getAccountContactFieldsForColumns(){
        columnDataForObjects = CCG_ConstructDataForSpenderSelection.getAccountContactFieldsForColumns();
    }
    
    /**
    * @description this methos is called when page loads and provide Raw content data for FilterFieldsTab.
    */
    @RemoteAction
    public static string allAboutObjectSpecificFilter(){
        return CCG_ConstructDataForSpenderSelection.allAboutObjectSpecificFilter();
    }
    
    @RemoteAction
    public static void addObject(string addRemoveObject,String accField,String operation){
       system.debug('test :;' +addRemoveObject+' :: '+accField+'  :: '+operation);
       if(operation=='Insert'){
           Spender_Selektion_Object__c setObject = new Spender_Selektion_Object__c();
           setObject.Name = addRemoveObject;
           setObject.Account_Field__c = accField;
           setObject.Object_Name__c = addRemoveObject;
           upsert setObject Object_Name__c;
       }else if(operation=='Delete'){
            List<Spender_Selektion_Object__c> lst = [select id from Spender_Selektion_Object__c where Name=:addRemoveObject];
            delete lst;
       }
    }
    
    @RemoteAction
    public static String showFields(String addRemoveObject){
        SObjectType sobjType = Schema.getGlobalDescribe().get(addRemoveObject);
        
        List<CCG_FieldDetailWrapper> lstOfUnselectedFields = new List<CCG_FieldDetailWrapper>();
        
        Map<String,Schema.SObjectField> mfields = sobjType.getDescribe().fields.getMap();
        for(string str : mfields.keySet()){
            if(mfields!=null && mfields.get(str).getDescribe().isFilterable() && !(String.valueof(mfields.get(str).getDescribe().getType())=='Address') && (String.valueof(mfields.get(str).getDescribe().getType())=='Reference') && mfields.get(str).getDescribe().getReferenceTo().size()>0 && mfields.get(str).getDescribe().getReferenceTo()[0].getDescribe().getName()=='Account'){
                string fApiName = String.valueof(mfields.get(str).getDescribe().getName());
                string fLabel = String.valueof(mfields.get(str).getDescribe().getLabel());
                
                lstOfUnselectedFields.add(new CCG_FieldDetailWrapper(fApiName,fLabel));
            }
        }
        if(addRemoveObject.equalsIgnoreCase('Account')){
            lstOfUnselectedFields.add(new CCG_FieldDetailWrapper('Id','Account-ID'));
        }
        
        return JSON.serialize(lstOfUnselectedFields);
    }
    
    /**
    * @description here we are saving filter fields , selected from multi-select picklist per object.
    * @param filterFields, Filter Fields Per Object in JSON form.
    */
    @RemoteAction
    public static string saveFilterFields(string filterFields){
        system.debug('filterFields :: '+filterFields);
        CCG_DmlManager.saveFilterFields(filterFields);
        // CCG_DmlManager.saveFilterFieldsClone(filterFields);
        return 'Successful';
    }
    
    /**
    * @description here we are saving Columns Related  Data .
    * @param columns, Columns data per row in JSON form.
    */
    @RemoteAction
    public static string saveColumns(string columns){
        system.debug('saveColumns :: '+columns);
        CCG_DmlManager.saveColumns(columns);
        return 'Successful';
    }
}