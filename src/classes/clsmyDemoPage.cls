public class clsmyDemoPage {
     public list<SelectOption>lst {get;set;}
    public string strSelectedAcc{get;set;}
    public clsmyDemoPage()
    {
        strSelectedAcc = '';
        lst = new list<SelectOption>();
        for(Account obj : [select Id,Name from Account])
        {
            lst.add(new SelectOption(obj.Id,Obj.Name));
        }
    }
}