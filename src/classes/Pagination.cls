public class Pagination {
    public integer totalRecs {get;set;} 
    public integer OffsetSize = 0;
    public integer LimitSize= 30;
    public Pagination(){
        totalRecs = 0; 
        totalRecs = [select count() from account];
        
    }
    public List<account> getacclist(){
        List<account> acc = [SELECT Name, website, AnnualRevenue, description, Type FROM account order by createdDate DESC LIMIT :LimitSize OFFSET :OffsetSize];
        
        System.debug('Values are'+ acc);
        return acc;
    }
    public void FirstPage(){
        OffsetSize = 0;
    }
    public void previous(){
        OffsetSize = OffsetSize - LimitSize;
    }
    public void next(){
       
        OffsetSize = OffsetSize + LimitSize;
    }
    public void LastPage(){
        if(math.mod(totalRecs,LimitSize)==0){
            OffsetSize = totalrecs - LimitSize;
        }else{
            OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
        }
        
    }
    public boolean getprev(){
        if(OffsetSize == 0)
        return true;
        else
        return false;
    }
    public boolean getnxt(){
        if((OffsetSize + LimitSize) >= totalRecs)
        return true;
        else
        return false;
    }
}