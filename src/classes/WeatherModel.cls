public class WeatherModel{
	public cls_data data {get;set;}
	public class cls_data {
		public cls_request[] request{get;set;}
		public cls_weather[] weather{get;set;}  
	}
	public class cls_request {
		public String type{get;set;}	//City                        
		public String query{get;set;}	//Delhi, India
	}
	public class cls_weather {
		public String weathetDate;	//2016-03-16    
		public cls_astronomy[] astronomy{get;set;}
		
		public String maxtempC{get;set;}
		public String maxtempF;	//89
		public String mintempC{get;set;}	//19
		public String mintempF;	//65
		public String uvIndex;	//0
		public cls_hourly[] hourly{get;set;}
	}
	public class cls_astronomy {
		public String sunrise{get;set;}	//07:30 AM
		public String sunset{get;set;}	//07:31 PM
		public String moonrise;	//01:17 PM
		public String moonset;	//02:14 AM
	}
	public class cls_hourly {
		public String weathertime{get;set;}	//230
		public String tempC	{get;set;}//19
		public String tempF;	//66
		public String windspeedMiles;	//9
		public String windspeedKmph{get;set;}	//14
		public String winddirDegree;	//342
		public String winddir16Point;	//NNW
		public String weatherCode;	//113
		public cls_weatherIconUrl[] weatherIconUrl;
		public cls_weatherDesc[] weatherDesc{get;set;}
		public String precipMM;	//0.0
		public String humidity{get;set;}	//45
		public String visibility{get;set;}	//10
		public String pressure;	//1013
		public String cloudcover;	//12
		public String HeatIndexC;	//19
		public String HeatIndexF;	//66
		public String DewPointC;	//6
		public String DewPointF;	//44
		public String WindChillC;	//19
		public String WindChillF;	//66
		public String WindGustMiles;	//18
		public String WindGustKmph;	//29
		public String FeelsLikeC;	//19
		public String FeelsLikeF;	//66
	}
	public class cls_weatherIconUrl {
		public String value;	//http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png
	}
	public class cls_weatherDesc {
		public String value{get;set;}	//Clear 
	}
	public static WeatherModel parse(String json){
		return (WeatherModel) System.JSON.deserialize(json, WeatherModel.class);
	}
}