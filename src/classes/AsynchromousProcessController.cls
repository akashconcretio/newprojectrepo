public class AsynchromousProcessController { 
    @future 
    public static void proceedAsynchronously(String className,String triggerOperation){
        
        TDTMInterface allInOneInterface = CreateHandler.returnHandler(className);
        if(triggerOperation == 'BeforeInsert'){
            system.debug(className+' @future :: '+'onBeforeInsert');
            
            allInOneInterface.onBeforeInsert(RunOnce.newList,RunOnce.oldList);
        }else if(triggerOperation == 'BeforeUpdate'){
            system.debug(className+' @future :: '+'onBeforeUpdate');
            
            allInOneInterface.onBeforeUpdate(RunOnce.newList,RunOnce.oldList);
        }else if(triggerOperation == 'AfterUpdate'){
            system.debug(className+' @future :: '+'onAfterUpdate');
            
            allInOneInterface.onAfterUpdate(RunOnce.newList,RunOnce.oldList);
        }else if(triggerOperation == 'AfterInsert'){
            system.debug(className+' @future :: '+'onAfterInsert');
            
            allInOneInterface.onAfterInsert(RunOnce.newList,RunOnce.oldList);
        }
    }
}