global with sharing class AccountRemoter {

    public String accountName { get; set; }
    public String accountPhone { get; set; }
    public static list<Account> account { get; set; }
    // public AccountRemoter() { } // empty constructor
    public static string strJson{get;set;}
    global AccountRemoter(){
        account = [SELECT Id, Name, Phone, Type, NumberOfEmployees,end_date__c,CreatedDate FROM Account limit 20];
        strJson = string.valueof(JSON.serialize(account));
        system.debug('strJson::'+strJson);
    }
    
    @RemoteAction
    global static String getAccount() {
        account = [SELECT Id, Name, Phone, Type, NumberOfEmployees,end_date__c,CreatedDate FROM Account limit 20];
        strJson = string.valueof(JSON.serialize(account));
        system.debug('strJson::'+strJson);
        return strJson;
        // return '{"Country":"India","state":[{"UP":"123"},{"Mp":"456"}]}';
        // system.debug('val:::'+accountPhone);
        // Account acc = new Account();
        // acc.Name = accountName;
        // acc.Phone = accountPhone;
        // acc.Text_Field__c = 'akash';
        // insert acc;
        // return acc;
    }
}