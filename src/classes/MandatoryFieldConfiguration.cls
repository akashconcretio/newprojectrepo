public class MandatoryFieldConfiguration {
    public list<CollectMandatoryField> getMandatoryFields{get;set;}
    public boolean allowEditing{get;set;}
    public MandatoryFieldConfiguration(){
        allowEditing = false;
        getMandatoryFields = new list<CollectMandatoryField>{new CollectMandatoryField(new contact()),
                new CollectMandatoryField(new Account()),new CollectMandatoryField(new Lead())};
        
        populateDefaultvalues(getMandatoryFields);
    }
    // Saves the Default Value for Mandatory Fields If Available in an Sobject. 
    public pagereference save(){
        list<Custom_Mandatory_Field_Configuration__c> lstNewRequiredField = new list<Custom_Mandatory_Field_Configuration__c>();
        for(CollectMandatoryField req:getMandatoryFields){
            for(MandatoryFieldWrapper mandatory :req.MandatoryFieldWrapperList){
                // if(!string.isEmpty(mandatory.defaultValue)){
                    Custom_Mandatory_Field_Configuration__c newRequiredFieldRecord = new Custom_Mandatory_Field_Configuration__c();
                    newRequiredFieldRecord.name = mandatory.fieldApiName;
                    newRequiredFieldRecord.Field_Api_Name_del__c = mandatory.fieldApiName;
                    newRequiredFieldRecord.Default_Value__c= (mandatory.defaultValue!=null || mandatory.defaultValue!='')?mandatory.defaultValue:null;
                    newRequiredFieldRecord.Type__c = req.objectType;
                    
                    lstNewRequiredField.add(newRequiredFieldRecord);
                // }
            }
        }
        if(lstNewRequiredField.size()>0){
            upsert lstNewRequiredField Field_Api_Name_del__c;
        }
        return new pagereference('/apex/MandatoryFieldConfiguration').setRedirect(true);
    }
    // This Method Populate The Default values If filled for the Required Fileds on VF Page from Sobject where It would be stored...
    public void populateDefaultvalues(list<CollectMandatoryField> getMandatoryFields){
        map<string,list<Custom_Mandatory_Field_Configuration__c>> mapForMandatory_Field_Configuration = new 
                                                                    map<string,list<Custom_Mandatory_Field_Configuration__c>>();
        for(Custom_Mandatory_Field_Configuration__c config : [select id ,Name,Default_Value__c,type__c from
                                                                Custom_Mandatory_Field_Configuration__c]){
            if(!mapForMandatory_Field_Configuration.containskey(config.type__c)){
                mapForMandatory_Field_Configuration.put(config.type__c,new list<Custom_Mandatory_Field_Configuration__c>{config});
                // system.debug('yes::2'+mapForMandatory_Field_Configuration);
            }else{
                mapForMandatory_Field_Configuration.get(config.type__c).add(config);
                
            }
        }
        for(CollectMandatoryField mergeValueIndefaultValue : getMandatoryFields){
            for(MandatoryFieldWrapper mandatory :mergeValueIndefaultValue.MandatoryFieldWrapperList){
                
                if(mapForMandatory_Field_Configuration.containsKey(mergeValueIndefaultValue.objectType)){
                    system.debug('akas::'+mapForMandatory_Field_Configuration.get(mergeValueIndefaultValue.objectType));
                    for(Custom_Mandatory_Field_Configuration__c assignValue :mapForMandatory_Field_Configuration.get(mergeValueIndefaultValue.objectType)){
                        system.debug('yes::2');
                        if(string.valueof(assignValue.name).equalsIgnoreCase(mandatory.fieldApiName)){
                            system.debug('yes::3');
                            mandatory.defaultValue = string.valueof(assignValue.Default_Value__c);
                        }
                    }
                }
            }
        }
    }
    //Wrapper Class that contains Mandatory Fields as per the Sobjects....
    public class CollectMandatoryField{
        public string objectType{get;set;}
        public Sobject objectForRequiredField{get;set;}
        public list<MandatoryFieldWrapper> MandatoryFieldWrapperList{get;set;}
        
        public CollectMandatoryField(Sobject mandatoryFiled){
            MandatoryFieldWrapperList = new list<MandatoryFieldWrapper>();
            objectForRequiredField = mandatoryFiled;
            objectType = string.valueof(mandatoryFiled.getSObjectType().getDescribe().getName());
            system.debug('objectType::'+objectType);
            extractRequiredFieldApiName();
        }
        // This Method Extracts Mandatory Field's Api Name,Label and Datatype.
        public void extractRequiredFieldApiName(){
            Schema.DescribeSObjectResult r = objectForRequiredField.getSObjectType().getDescribe();
            map<String,string> requiredFiledApiNames =  new map<String,string>();
            for(string apiName : r.fields.getMap().keySet()){
              Schema.SObjectField field = r.fields.getMap().get(apiName);
              Schema.DescribeFieldResult F = field.getDescribe();
              Boolean isFieldreq  = F.isNillable();
              if(!isFieldreq){
                  requiredFiledApiNames.put(apiName,F.getLabel());
                  if(apiName.contains('__c')){
                      MandatoryFieldWrapperList.add(new MandatoryFieldWrapper(string.valueof(apiName) , string.valueof(F.getLabel()), string.valueof(F.getType())));
                  }
              }
            }
            system.debug('requiredFiledApiNames::'+requiredFiledApiNames);
        }
    }
    public pagereference cancel(){
        return new pagereference('/apex/MandatoryFieldConfiguration').setRedirect(true);
    }
    public void edit(){
        allowEditing = true;
    }
    
    //fill the value in mandatory field when trigger executes......
    public void configureMandatoryFieldForNewRecords(list<sobject> sobj){
        Schema.DescribeSObjectResult r = sobj[0].getSObjectType().getDescribe();
        list<string> lstOfString = new list<string>();
        string objType = sobj[0].getSObjectType().getDescribe().getName();
        map<string,map<string,string>> collectAllMandatoryFieldInMap = new map<string,map<string,string>>();
        for(Custom_Mandatory_Field_Configuration__c man : [select Type__c,Field_Api_Name_del__c,Default_Value__c from Custom_Mandatory_Field_Configuration__c where type__c =:objType]){
            if(collectAllMandatoryFieldInMap.containskey(string.valueof(man.Type__c))){
                collectAllMandatoryFieldInMap.get(string.valueof(man.Type__c)).put(man.Field_Api_Name_del__c,man.Default_Value__c);
            }else{
                map<string,string> requiredFieldDefaulValueMap = new map<string,string>();
                requiredFieldDefaulValueMap.put(string.valueof(man.Field_Api_Name_del__c),string.valueof(man.Default_Value__c));
                collectAllMandatoryFieldInMap.put(string.valueof(man.Type__c),requiredFieldDefaulValueMap);
            }
        }
        
        for(string apiName : r.fields.getMap().keySet()){
              Schema.SObjectField field = r.fields.getMap().get(apiName);
              Schema.DescribeFieldResult F = field.getDescribe();
              Boolean isFieldreq  = F.isNillable();
              if(!isFieldreq){
                //   requiredFiledApiNames.put(apiName,F.getLabel());
                  if(apiName.contains('__c')){
                      lstOfString.add(string.valueof(apiName));
                  }
              }
        }
        fillMandatoryFields(lstOfString,sobj,collectAllMandatoryFieldInMap);
    }
    // fill the values when trigger run in before event on any Sobject.
    public void fillMandatoryFields(list<string> lstOfString,list<sobject> sobj,map<string,map<string,string>> collectAllMandatoryFieldInMap){
        for(Sobject sb : sobj){
            for(string str : lstOfString){
                if(sb.get(str)==Null){
                    sb.put(str,string.valueof(collectAllMandatoryFieldInMap.get(sobj[0].getSObjectType().getDescribe().getName()).get(str)));
                 }
            }
        }
    }
    //Used To Display the mandatory Fields on VF Page..
    public class MandatoryFieldWrapper{
        public string fieldApiName{get;set;}
        public string fieldLabel{get;set;}
        public string defaultValue{get;set;}
        public string dataType{get;set;}
        
        public MandatoryFieldWrapper(string a, string b,string c){
            fieldApiName = a;
            fieldLabel   = b;
            dataType  = c;
        }
    }
}