public class SendAccountUsingRESTAPI {
    private final String clientId = '3MVG9d8..z.hDcPK5wx2w7nOTN2HbpiItTiIsuOaK4BekmVyeMt0vL_lbDu7xkL.RItmc0Umi8h0FfgQsb4za';
    private final String clientSecret = '1695160901651082133';
    private final String username = 'akash9602@concret.io';
    private final String password = 'concretiotesting927$qw2tFocDsMYyp872hLZn6l2Nm';
    public class deserializeResponse{
        public String id;
        public String access_token;
    }
    public String ReturnAccessToken (){
        String reqbody = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setBody(reqbody);
        req.setMethod('POST');
        req.setEndpoint('https://login.salesforce.com/services/oauth2/token');
        HttpResponse res = h.send(req);
        
        system.debug('@@@@res@@'+res.getBody());
        deserializeResponse resp1 = (deserializeResponse)JSON.deserialize(res.getbody(),deserializeResponse.class);
        system.debug('@@@@access_token@@'+resp1.access_token );
        return resp1.access_token;
    }
    public static string callgetContact (String accId){   
        SendAccountUsingRESTAPI acount1 = new SendAccountUsingRESTAPI();
        String accessToken;
        accessToken = acount1.ReturnAccessToken ();
        list<Contact> LstContact=new List<Contact>();
        String trimmedResponse;
        if(accessToken != null){
           String endPoint = 'https://ap5.salesforce.com/services/apexrest/v1/getContacts/' +accId;
           //String jsonstr = '{"accId" : "' + accId+ '"}';
           Http h2 = new Http();
           HttpRequest req1 = new HttpRequest();
           req1.setHeader('Authorization','Bearer ' + accessToken);
           req1.setHeader('Content-Type','application/json');
           req1.setHeader('accept','application/json');
           req1.setBody('{"attributes":{"type":"Contact","url":"/services/data/v42.0/sobjects/Contact/0037F00000ZsrHyQAJ"},"Id":"0037F00000ZsrHyQAJ","Name":"Jack Rogers","Phone":"(336) 222-7000","Fax":"(336) 222-8000","Email":"jrogers@burlington.com"}');
           req1.setMethod('POST');
           req1.setEndpoint(endPoint);
           HttpResponse res1 = h2.send(req1);
           trimmedResponse = res1.getBody().unescapeCsv().remove('\\');
           system.debug('@@@RESPONSE@@'+trimmedResponse);
           //Map<String, Object> tmpResult = (Map<String, Object>) JSON.deserializeUntyped(string.valueof(trimmedResponse));
           //system.debug('@@@tmpResult@@'+tmpResult);
           
        }
        return trimmedResponse;
    }
}