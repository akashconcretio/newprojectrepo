public class lightningFramework {
    @AuraEnabled
    public static boatTypeAndTheme getBoatTypeDetails(){
        return new boatTypeAndTheme([select name from BoatType__c limit 1000],UserInfo.getUiThemeDisplayed());
    }
    @AuraEnabled
    public static list<Boat__c> getSelectedBoats(String boatTypeId){
        return [select name from Boat__c limit 1000];
    }
    @AuraEnabled
    public static list<Boat__c> allBoats(){
        return [select id,Name from Boat__c limit 1000];
    }
    @AuraEnabled
    public static String getUIThemeDescription() {
        String theme = UserInfo.getUiThemeDisplayed();
        return theme;
    }
    
    public class boatTypeAndTheme{
        @AuraEnabled
        public List<BoatType__c> lstBoats;
        @AuraEnabled
        public string theming;
        public boatTypeAndTheme(List<BoatType__c> lstB,String theme){
            lstBoats = lstB;
            theming = theme;
        }
    }
}