global class AcccountsByRemoting {
    //Akash Pundir
    // public static List<Account> account{get;set;}
    public static string conJson{get;set;}
    public static String inputHid{get;set;}
    // global static List<string> lstOfApiName;
    public Boolean isSameInstance{get;set;}
    public List<getAllChildObject> allChildObj{get;set;}
    
    global AcccountsByRemoting(){
        allChildObj = new List<getAllChildObject>();
        isSameInstance = true;
        allChildObject();
    }
    
    public List<getAllChildObject> allChildObject(){
        
        Map<String,Schema.SObjectType> sObjects = Schema.getGlobalDescribe(); 
        Map<String,Schema.SObjectType> sObjectCustom = new Map<String, SObjectType>(); 
        for(String apiName : sObjects.keySet()){
            // system.debug('apiName::'+sObjects.get('apiName').getDescribe().isCustom());
            // if(sObjects.get('apiName').getDescribe().isCustom()){
            //     // sObjectCustom.put(apiName,sObjects.get(apiName));
            // }
        }
        // if(sObjectCustom!=null && sObjectCustom.size()>0){
        //     for (String apiName : sObjectCustom.keySet()){
        //         Map <String, Schema.SObjectField> fieldMap = sObjectCustom.get('apiName').getDescribe().fields.getMap();
        //         for(string str : fieldMap.keySet()){
        //             DescribeFieldResult res = fieldMap.get(str).getDescribe();
        //             // system.debug('apiName::'+res.getType());
        //             if(string.valueof(res.getType()).equalsIgnoreCase('REFERENCE')){
        //                 // system.debug('apiName::'+res.getReferenceTo());
        //                 if(string.valueof(res.getReferenceTo()).equals('(Account)')){
        //                 //   f.getReferenceTo() 
        //                     allChildObj.add(new getAllChildObject(apiName,string.valueof(sObjectCustom.get(apiName).getDescribe().getLabel())));
        //                 }
        //             }
        //         }
        //         // allChildObj.add(new getAllChildObject(apiName,string.valueof(sObjects.get(apiName).getDescribe().getLabel())));
        //     // }
        //     // for(string str : ){
                
        //     }
        // }
        system.debug('apiName::'+allChildObj);
        return null;
    }
    @RemoteAction
    global static List<Account> getAccount(String accountName,string rmList) {
        // system.debug('abc::+++'+accountName);
        if(rmList.equalsIgnoreCase('no') && !String.isEmpty(accountName)){
            string searchName = '%'+accountName+'%';
            List<Account> account = [SELECT Id, Name,Rating, Phone, Type, NumberOfEmployees 
                       FROM Account WHERE Name LIKE : searchName];
                       
            return account.size()>0?account:null;
        }else{
            return null;
        }
    }
    @RemoteAction
    global static returnChildObjectWithFieldSet getContact(string accid) {
        List<string> lstOfApiName = new List<string>();
        List<Schema.FieldSetMember> fm = readFieldSet('conf','Contact');
        
        for(Schema.FieldSetMember fs:fm){
            lstOfApiName.add(fs.getFieldPath());
        }
        list<contact> lstCon;
        if(lstOfApiName.size()>0){
            string query  = 'select '+string.join(lstOfApiName,',')+','+'LastName '+'from contact where accountId = :accid';
            lstCon = Database.query(query);
        }else{
            lstCon = [select LastName,CreatedDate from contact where accountId = :accid];
        }
        returnChildObjectWithFieldSet contactWitFS = new returnChildObjectWithFieldSet(lstCon!=null?lstCon:null,string.valueof(JSON.serialize(fm)));
        
        return contactWitFS;
    }
    global class returnChildObjectWithFieldSet{
         public List<contact> lstOfContact{get;set;}
         public string lstOfFieldSetMembers{get;set;}
         public returnChildObjectWithFieldSet(List<contact> con,string fs ){
             lstOfContact = con;
             lstOfFieldSetMembers = fs;
         }
    }
    
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
    
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    }  
    
    public class getAllChildObject{
        public string objectApiName{get;set;}
        public string objectLabelName{get;set;}
        public getAllChildObject(string apiName,String Label){
            objectApiName = apiName;
            objectLabelName = Label;
        }
    } 
}