public class wrapperClassController1 {

    public PageReference contactlist() {
        return null;
    }

    public string accid{get;set;}
    public String selectedAccId{get;set;}
    public string selectedconid{get;set;}
    public boolean showhide{get;set;}
    public boolean hide{get;set;}

    public wrapperClassController1()
    {
        accid= ApexPages.currentPage().getParameters().get('id');
        showhide=false;
        hide=true;
    }
    public void hiden()
    {
        hide=false;
    }
    public void show()
    {
        showhide=true;
        hide=true;
    }

     public List<SelectOption> getAccountNames()
    {
        List<SelectOption> accOptions= new List<SelectOption>();
        accOptions.add( new SelectOption('','--Select Account--'));
        for( Account acc : [select Id,name from Account ] ) 
        {
            accOptions.add( new SelectOption(acc.Id,acc.name)); 
        }
        show();
        return accOptions;
    }

    public List<cContact> contactList {get; set;}
    public List<cContact> getContacts() {
        if( selectedAccId != null && selectedAccId.length() > 0 ) 
        { 
            contactList = new List<cContact>();
              
            for(Contact c : [select Id, Name, Email, Phone from Contact where AccountId =:selectedAccId ])
            {
                contactList.add(new cContact(c));
            }
        }


        return contactList;
    }
    public PageReference processSelected() {
        List<Contact> selectedContacts = new List<Contact>();
        for(cContact cCon : contactList) {
            if(cCon.selected == true) {
                selectedContacts.add(cCon.con);
            }
        }
        System.debug('These are the selected Contacts…');
        for(Contact con : selectedContacts) {
            string conEmail = con.Email;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {conEmail};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('deepak@gmail.com');
            mail.setSenderDisplayName('Salesforce Support');
            mail.setSubject('New Case Created : ' + case.Id);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setPlainTextBody('Thank for Contacting');
            mail.setHtmlBody('Thank for Contacting');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        return null;
    }

    public class cContact {
        public Contact con {get; set;}
        public Boolean selected {get; set;}
        public cContact(Contact c)
        {
            con = c;
            selected = false;
        }
    }
}