public with sharing class LookUpCtrl {
    public LookUpCtrl() {

    }

    @AuraEnabled(cacheable = true)
    public static List<Account> lookUp(String searchKeyword,String objName,Integer maxResults){
        System.debug('logLevel'+ searchKeyword);
        searchKeyword = '%'+searchKeyword+'%';

       return Database.query('select id ,Name from '+objName+' where Name Like : searchKeyword limit :maxResults');

    }
}