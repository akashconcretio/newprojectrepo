public  class TeamWorkTaskConfiguration {
    public   TWTasksWrapper twTaskWrapper{get;set;}
    // public  TWTasksWrapper.TWProjectTasksList twTasksListWrapper{get;set;}
    public  list<SelectOption> lstOfProjects{get;set;}
    public  list<SelectOption> lstOfProjectsTasksList{get;set;}
    //public  list<SelectOption> lstOfTasksInTasksList{get;set;}
    public  string selected{get;set;}
    public   string selectedTasksList{get;set;}
    // public  string selectedTask{get;set;}
    public  integer showhide{get;set;}
    public  list<string> lstOfDesiredJsonForAllTasks;
    // public string selectedTaskDetails{get;set;}
    // public boolean visible{get;set;}
    // public string taskToBeUpdated{get;set;}
    public map<string,string> mapOfProjectIdName;
    public  map<string,string> mapOfTaskIdName{get;set;}
    public  map<string,string> mapOfTasksListIdName{get;set;}
    public static Boolean isCreateTask{get;set;}
    
    public TeamWorkTaskConfiguration(ApexPages.StandardController controller){   //  ApexPages.StandardController controller
        showhide  = 1;
        lstOfProjects = new list<SelectOption>();
        AllTeamworkProjects();
        isCreateTask = false;
    }
    
    public void AllTeamworkProjects(){  //This method calls all projects in a company. and put them in a picklist.
        mapOfProjectIdName = new map<string,string>();
        string response;
        HttpRequest req ;
        Http http;
        HTTPResponse res;
        TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
        String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
        req = new HttpRequest(); 
        req.setEndpoint(Configure.Endpoint__c+'projects.json');
        req.setMethod('GET');
        req.setHeader('Accept', 'application/json; charset=UTF-8');
        req.setHeader('Content-Type','application/json');  
        req.setHeader('Authorization', 'Basic '+authorizationHeader);
        req.setTimeout(120000);
        http = new Http();
        res = http.send(req);
        response = string.valueof(res.getBody()).replaceAll('-','_');
        twTaskWrapper = TWTasksWrapper.parse(response);
        //system.debug('resp:::'+response);
        lstOfProjects.add(new SelectOption('None','Select Project'));
        for(TWTasksWrapper.cls_projects tsk : twTaskWrapper.projects){
            lstOfProjects.add(new SelectOption(tsk.id,tsk.name));
            mapOfProjectIdName.put(tsk.id,tsk.name);
        }
    }
    public void ProjectsAllTaskLists(){      //This method calls all taskslist under a given project in a company. and put them in a picklist.
       if(selected!='None'){
            mapOfTasksListIdName = new map<string,string>();
            showhide  = 2;
            lstOfProjectsTasksList = new list<SelectOption>();
            string response;
            HttpRequest req ;
            Http http;
            HTTPResponse res;
            TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
            String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
            req = new HttpRequest(); 
            req.setEndpoint(Configure.Endpoint__c+'projects/'+selected+'/tasklists.json');
            //system.debug('TList::'+req.getEndpoint());
            req.setMethod('GET');
            req.setHeader('Accept', 'application/json; charset=UTF-8');
            req.setHeader('Content-Type','application/json');  
            req.setHeader('Authorization', 'Basic '+authorizationHeader);
            req.setTimeout(120000);
            http = new Http();
            res = http.send(req);
            response = string.valueof(res.getBody()).replaceAll('-','_').replaceAll('private','privateList');
            twTaskWrapper = TWTasksWrapper.parseTasksList(response);
            lstOfProjectsTasksList.add(new SelectOption('None','Select TaskList'));
            //system.debug('TList::'+twTaskWrapper.tasklists[0].name);
            if(twTaskWrapper.tasklists.size()!=0){
                for(TWTasksWrapper.cls_tasklists taskList : twTaskWrapper.tasklists){
                    lstOfProjectsTasksList.add(new SelectOption(taskList.id,taskList.name));
                    mapOfTasksListIdName.put(taskList.id,taskList.name);
                }
            }
       }else {
           showhide  = 1;
       }
    }
    public  void AllTasksInTasksList(){   // This method calls all tasks under a given taskslist in a company. and put them in a picklist.
        if(selectedTasksList!='None'){
            showhide = 3;
            mapOfTaskIdName = new map<string,string>();
            lstOfDesiredJsonForAllTasks = new list<string>();
            string response = '';
            HttpRequest req ;
            Http http;
            HTTPResponse res;
            TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
            String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
            req = new HttpRequest(); 
            req.setEndpoint(Configure.Endpoint__c+'tasklists/'+selectedTasksList+'/tasks.json');
            system.debug('TList::'+req.getEndpoint());
            req.setMethod('GET');
            req.setHeader('Accept', 'application/json; charset=UTF-8');
            req.setHeader('Content-Type','application/json');  
            req.setHeader('Authorization', 'Basic '+authorizationHeader);
            req.setTimeout(120000);
            http = new Http();
            res = http.send(req);
            list<string> lstAllTasksInTL = string.valueof(res.getBody()).replaceAll('-','_').replaceAll('private','privateList').split(',"DLM":');  //A List That contains Error Free Json for parsing..
            for(integer i=1;i<=lstAllTasksInTL.size()-1;i++){
              lstOfDesiredJsonForAllTasks.add(lstAllTasksInTL[i].substring(13,lstAllTasksInTL[i].length()));   // ----------++++++++>>>>>  A list that does't contain DLM Variable in json that causes a problem for parser.
            }
            response = lstAllTasksInTL[0]+string.join(lstOfDesiredJsonForAllTasks,'');
            //system.debug('response::++'+response);
            twTaskWrapper = TWTasksWrapper.parseTasksINTasksList(response);      //-----------------++++++++++  Parsing In The Wrapper Class...
            for(TWTasksWrapper.cls_todo_items tsk : twTaskWrapper.todo_items){
                mapOfTaskIdName.put(string.valueof(tsk.id),tsk.content);
            }
            //teamworkProjectInsertion();
        }else {
            showhide = 2;
        }
    }
    public Pagereference teamworkProjectInsertion(){
            isCreateTask = false;  // used in task trigger when tasks inserted from api call then trigger will not execute.
        //if(selected!=null && selectedTasksList!=null){
            list<TeamWork_Project__c> twProjectsToBeDeleted = [select id from TeamWork_Project__c];
            if(twProjectsToBeDeleted.size() > 0){
                delete twProjectsToBeDeleted;
            }
            TeamWork_Project__c twp = new TeamWork_Project__c();
            twp.Name = mapOfProjectIdName.get(selected);
            twp.Project_Id__c = selected;
            twp.Tasklists_Id__c = selectedTasksList;
            twp.TaskList_Name__c = mapOfTasksListIdName.get(selectedTasksList);
            upsert twp Project_Id__c;
            
            string twProjectObjId = [select id from TeamWork_Project__c limit 1].Id;
            //system.debug('twProjectObjId::'+twProjectObjId);
            list<task> lstOfTasksToBeDeleted = [select id from task where whatId =:selected];
            delete lstOfTasksToBeDeleted;
            List<Task> lstOfTask = new List<Task>();
            if(mapOfTaskIdName.size()>0){
                for(string lststr:mapOfTaskIdName.keyset()){
                    Task tsk = new Task();
                    tsk.ownerid = UserInfo.getUserId();
                    tsk.Task_Id__c = lststr;
                    tsk.subject = mapOfTaskIdName.get(lststr);
                    tsk.Status  = 'in Progress';
                    tsk.Priority = 'low';
                    tsk.whatId = twProjectObjId;
                    lstOfTask.add(tsk);
                }
            }
            if(lstOfTask.size()>0){
                upsert lstOfTask Task_Id__c;
            }
            String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
            Pagereference pgref = new Pagereference(baseUrl+'/'+twProjectObjId);
            pgref.setRedirect(true);
        return pgref;
    }
}