public class clsMyController1 {
public Contact objCon{get;set;}    
    public boolean isAdminUser{get;set;}    
    public boolean isEditMode{get;set;}    
    
    public clsMyController1()
    {
        objCon = new Contact();
        isAdminUser = false;
        isEditMode = true;
        
        list<User> lstUser = [select Id,Name,Profile.Name from User where Id =: Userinfo.getUserId()];
        if(lstUser != null && lstUser.size() > 0)
        {
            if(lstUser[0].Profile.Name.EqualsIgnoreCase('System Administrator'))
            {
                isAdminUser = true;
            }
        }
    }
    
    public void mySave()
    {
        if(objCon.Email != null && objCon.Email.trim() !='')
        {
            upsert objCon;
            isEditMode = false;
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter valid email id'); 
            ApexPages.addMessage(myMsg);
        }
    }
    
    public void myReset()
    {
        objCon = new Contact();  
    }
    
    public void myEdit()
    {
        //objCon = new Contact();  
        isEditMode = true;
    }
}