public class UpdateAllContactsAddrFromFlow {
    @InvocableMethod
    public static List<Contact> returnUpdatedContacts(List<inputFromFlow> inputFromFlow) {
        
        for (Contact contact : inputFromFlow[0].lstCon) {
            contact.mailingStreet = inputFromFlow[0].acc.BillingStreet;
        }
        return inputFromFlow[0].lstCon;
    }
    
    
    public class inputFromFlow{
        @InvocableVariable
        public List<Contact> lstCon;
        @InvocableVariable
        public Account acc;
    }
}