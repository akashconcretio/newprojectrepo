@isTest
global class AnimalLocatorMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"animal":{"id":99,"name":"trailhead","eats":"burritos","says":"more badgers"}}');
        res.setStatusCode(200);
        return res;
    }
}