/**
* @author Badan Singh Pundeer
* @date 29.05.2018
*
* @group CBM
*
* @description wrapper class that exposes the field api name and label. those are available on multi select pick-list to set filter fields.
*
* @history
* version                           | author                                    | changes
*=============================================================================
* 1.0 (29.05.2018)          | Badan Pundeer                   | initial version
*/
public class CCG_FieldDetailWrapper {
	public string fieldApiName;
    public string fieldLabel;
    public CCG_FieldDetailWrapper(String fieldApiName,String fieldLabel){
        this.fieldApiName = fieldApiName;
        this.fieldLabel = fieldLabel;
    }
}