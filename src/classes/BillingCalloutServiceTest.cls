@isTest
private class BillingCalloutServiceTest {

	private static testMethod void test() {
	    
	    Opportunity opp = new Opportunity();
	    opp.Name = 'opp';
	    opp.CloseDate = Date.newInstance(2018,01,20);
	    opp.stageName = 'prospecting';
	    insert opp;
	    
	    Project__c prj = new Project__c();
	    prj.Name = 'Test';
	    prj.Billable_Amount__c = 100;
	    prj.End_Date__c = date.newInstance(2018,01,21);
	    prj.Start_Date__c = date.newInstance(2018,01,20);
	    prj.Opportunity__c = opp.Id;
	    prj.ProjectRef__c = 'sdhbdh';
	    prj.Status__c = 'Billable';
	    insert prj;
	    
	    Project__c prjct = [select id,Status__c from Project__c limit 1];
	    prjct.Status__c = 'Billable';
	    
	    Test.setMock(HttpCalloutMock.class, new BillingCalloutServiceMock());
	    Test.startTest();
	            update prjct;
        Test.stopTest();
	    
	}

}