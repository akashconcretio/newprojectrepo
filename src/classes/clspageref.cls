public class clspageref {
        public boolean showhide{get;set;}
        public boolean showhide1{get;set;}
        public boolean showhide2{get;set;}
        public teacher__c insrt1{get;set;}
        public product__c insrt{get;set;}
 public clspageref()
        { showhide=false;
          showhide1=false;
          showhide2=false;
          insrt=new product__c();
          insrt1=new teacher__c();
        }
 public void show()
      {  
         if(insrt.Price__c!= null)
             { 
               insert insrt;
                 
               showhide2=true;
               showhide=false;
               showhide1=false;
             }
         else
             {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Price'); 
                ApexPages.addMessage(myMsg);
             }
      }
 public void show1()
        {  
           insert insrt1;
        }
 public void proddetails()
        {
          showhide=true;
          showhide1=false;
          showhide2=false;
          
          
        }
 public void tecdetails()
        {
           showhide=false;
           showhide1=true;
           showhide2=false;
        }
 public void reset()
        {
            insrt=new product__c();
        }
 public void edit()
 {
     showhide=true;
     showhide2=false;
  }
   
   }