@isTest
private class WarehouseSyncScheduleTest {

	private static testMethod void testWarehouseSyncSchedule() {
	    Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());
	    Test.startTest();
	    
	        system.schedule('test','0 0 01 * * ? *',new WarehouseSyncSchedule());
	    Test.stopTest();
	}

}