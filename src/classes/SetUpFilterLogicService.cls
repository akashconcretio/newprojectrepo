/**
* @author Badan Singh Pundeer
* @date 20.05.2018
*
* @group CBM
*
* @description set Up filter logic.
*             
*
* @history
* version                           | author                                    | changes
*=============================================================================
* 1.0 (31.05.2018)          | Badan Pundeer                   | initial version
*/

public class SetUpFilterLogicService{
    public SetUpFilterLogicService(){
        
    }
    
    /**
    * @description set the filter logic as per object.
    * @param mapOfFilterLogic, this map holds filter logic given by user per object. like new Map<string,Object>{'Account'=>'(1 AND 2)'}  ;  
    * 
    * @param objectFiltersNumberSorted, this map finally will contain all filter rows per object.
    *   @return returns map of object and complete filter string.
    * 
    */
    public static Map<string,string> returnFilterStringAsPerFilterLogic(Map<string,Object> mapOfFilterLogic,Map<String,Map<integer,String>> objectFiltersNumberSorted){
        Map<string,string> mapStr = new Map<string,string>();
        Boolean foundFilter = false;
        
        system.debug('mapOfFilterLogic  :  '+mapOfFilterLogic);
        system.debug('objectFiltersNumberSorted  :  '+objectFiltersNumberSorted);
        
        for(string filterLogicObj: objectFiltersNumberSorted.keySet()){
            String filterLogicString = mapOfFilterLogic.get(filterLogicObj)!=''?String.valueof(mapOfFilterLogic.get(filterLogicObj)):'';
            if(filterLogicString!='' && filterLogicString!=null){
                filterLogicString = filterLogicString.replaceAll('OR',' OR ').replaceAll('AND',' AND ').replaceAll('NOT',' NOT ');
                system.debug('filterLogicStringInit  :  '+filterLogicString);
                foundFilter = true;
            }else{
                foundFilter = false;
            }
            
            for(integer mapOfFilterSequence :objectFiltersNumberSorted.get(filterLogicObj).keySet()){
                if(filterLogicString!='' && filterLogicString!=null && foundFilter){
                    system.debug('kisme  :  '+String.valueof(mapOfFilterSequence));
                    system.debug('kya :: '+objectFiltersNumberSorted.get(filterLogicObj).get(mapOfFilterSequence));
                    
                    Integer lastOccuranceOfRowCount = filterLogicString.lastIndexOf(String.valueof(mapOfFilterSequence));
                    
                    String subStringBeforeLastOccurance = filterLogicString.substring(0,lastOccuranceOfRowCount);
                    String subStringAfterLastOccurance = filterLogicString.substring(lastOccuranceOfRowCount,filterLogicString.length());
                    
                    filterLogicString = subStringBeforeLastOccurance+subStringAfterLastOccurance.replace(String.valueof(mapOfFilterSequence),objectFiltersNumberSorted.get(filterLogicObj).get(mapOfFilterSequence));
                    
                    system.debug('val$$%%  :  '+objectFiltersNumberSorted.get(filterLogicObj).get(mapOfFilterSequence));
                    system.debug('subStringAfterLastOccurance  :  '+subStringAfterLastOccurance);
                    
                    
                    // filterLogicString = filterLogicString.replace(String.valueof(mapOfFilterSequence),objectFiltersNumberSorted.get(filterLogicObj).get(mapOfFilterSequence));
                    
                    system.debug('filterLogicString  :  '+filterLogicString);
                }else{
                    filterLogicString+=objectFiltersNumberSorted.get(filterLogicObj).get(mapOfFilterSequence)+' AND ';
                }
                system.debug('filterLogicString  :  '+filterLogicString);
            }
            filterLogicString = filterLogicString.removeEnd(' AND ');
            
            mapStr.put(filterLogicObj,filterLogicString);
        }
        
        return mapStr;
    }
}