@isTest
private class UpdateActivityTest {
    @testSetup static void dataSetup(){
        Opportunity opp = new Opportunity(Name ='Test opp', StageName ='1 Month', Opportunity_Size__c ='> 25M', CloseDate = Date.today());
        insert opp;
        Task tsk = new Task( whatid =opp.Id);
        insert tsk;
        Event evnt = new Event(whatid =opp.Id, StartDateTime=Date.newInstance(2016, 2, 20), EndDateTime=Date.newInstance(2016, 2, 20));
        insert evnt;
    }
	private static testMethod void testUpdateActivitiesonOpp() {
       //1. Test Opportunity_Size__c ='> 25M' and StageName ='1 Month'
       Opportunity opp =[SELECT id, StageName FROM Opportunity];
       update opp;
       Task tsk =[SELECT Priority FROM Task WHERE whatid =:opp.Id];
       Event evnt =[SELECT priority__c FROM Event WHERE whatid =:opp.Id];
       System.assertEquals('1', tsk.Priority);
       System.assertEquals('1', evnt.priority__c); 
       
       //2. Test Opportunity_Size__c ='> 25M' and StageName ='1 Month'
       opp.Opportunity_Size__c = '< 25M';
       opp.Stagename = '1 Month';
       Update opp;
       
       //3. Test Opportunity_Size__c ='< 25M' and StageName ='6 Month'
       opp.Stagename = '6 Month';
       Update opp;
       
       //4. Other
       opp.Stagename = 'Closed Won';
       Update opp;
    }

}