public class CampingListController {
    
    @AuraEnabled
    public static List<Camping_Item__c> getItems(){
        return [select id,Name,Price__c,Quantity__c,Packed__c from Camping_Item__c];
    }
    @AuraEnabled
    public static void saveItem(Camping_Item__c camp){
        system.debug('camp  : '+camp);
        insert camp;
    }
}