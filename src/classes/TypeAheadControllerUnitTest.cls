@IsTest(seealldata = false)
public class TypeAheadControllerUnitTest {
    
    public static FINAL String SEARCH_TERM = 'Amit';
    @TestSetup
    public static void setUpData() {
        Campaign cmp = New Campaign(Name='Test Campaign', Description ='This is demo Campaign ');
        insert cmp;
        Account acc = new Account(Name ='Test Account');
        insert acc;
        List<Lead> leadList = new List<Lead>();
        for( Integer i=0; i<10; i++) {
            Lead lead = new Lead(LastName='Singh', FirstName = 'Amit', Company = acc.Name,
                                 Email ='amitsingh@gmail.com', Phone='0987654321');
            leadList.add(lead);
        }
        insert leadList;
    }
    
    public static testMethod void constructortest() {
        List<Campaign> campList = [Select Id, Name From Campaign Limit 1];
        if ( !campList.isEmpty() ) {
            Test.setCurrentPage(Page.TypeAheadVF );
            ApexPages.currentPage().getParameters().put('id', campList.get(0).Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(campList.get(0));
            Test.startTest();
            TypeAheadController contr = new TypeAheadController(controller);
            contr.searchTerm   = SEARCH_TERM;
            contr.selectedLead = SEARCH_TERM;
            String campaignId = contr.campaignId;
            System.assertEquals( campList.get(0).Id , campaignId , 'Campaign Id does not Match!');
            Test.stopTest();
        }
    }
    
    public static testMethod void typeAheadTest() {

        Test.startTest();
        List<Lead> leadList = [Select Id, Name, Email From Lead Limit 100];
        List<String> leadIds = new List<String>();
        for ( Lead l : leadList ) {
            leadIds.add(l.Id);
        }
        Test.setFixedSearchResults( leadIds );
        TypeAheadController.getSearchSuggestions('amit');
        Test.stopTest();

    }
    
    public static testMethod void associateLeadTest() {
        List<Campaign> campList = [Select Id, Name From Campaign Limit 1];
        if ( !campList.isEmpty() ) {
            Test.setCurrentPage(Page.TypeAheadVF );
            ApexPages.currentPage().getParameters().put('id', campList.get(0).Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(campList.get(0));
            TypeAheadController contr = new TypeAheadController(controller);
            List<Lead> leadList = [Select Id, Name, Email From Lead Limit 100];
            String leadIds = '';
            for( Lead l : leadList ) {
                leadIds += l.Id+',';
            }
            leadIds = leadIds.substringBeforeLast(',');
            
            Test.startTest();
            	contr.leadIdsSet = leadIds;
            	contr.Associate();
            	List<CampaignMember> cmpMemberList = [Select Id, LeadId from CampaignMember Where CampaignId =:campList.get(0).Id ];
            	System.assertEquals( leadList.size() , cmpMemberList.size() , '# of Campaign Members are not Matching !' );
                contr.leadIdsSet = '';
            	contr.Associate();
                List<CampaignMember> cmpMemberListAfter = [Select Id, LeadId from CampaignMember Where CampaignId =:campList.get(0).Id ];
            	System.assertEquals( leadList.size() , cmpMemberListAfter.size() , '# of Campaign Members are not Matching !' );
            Test.stopTest();
        }
    }
    
    public static testMethod void associateLeadErrorTest() {
        List<Campaign> campList = [Select Id, Name From Campaign Limit 1];
        if ( !campList.isEmpty() ) {
            Test.setCurrentPage(Page.TypeAheadVF );
            ApexPages.currentPage().getParameters().put('id', campList.get(0).Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(campList.get(0));
            TypeAheadController contr = new TypeAheadController(controller);
            List<Lead> leadList = [Select Id, Name, Email From Lead Limit 100];
            String leadIds = '';
            for( Lead l : leadList ) {
                leadIds += l.Id+',';
            }
            leadIds = leadIds.substringBeforeLast(',');
            
            Test.startTest();
            	contr.leadIdsSet = leadIds;
            	contr.Associate();
                List<CampaignMember> cmpMemberList = [Select Id, LeadId from CampaignMember Where CampaignId =:campList.get(0).Id ];
            	System.assertEquals( leadList.size() , cmpMemberList.size() , '# of Campaign Members are not Matching !' );
            	contr.Associate();
            	List<CampaignMember> cmpMemberListAfter = [Select Id, LeadId from CampaignMember Where CampaignId =:campList.get(0).Id ];
            	System.assertEquals( leadList.size() , cmpMemberListAfter.size() , '# of Campaign Members are not Matching !' );
            Test.stopTest();
        }
    }
    
}