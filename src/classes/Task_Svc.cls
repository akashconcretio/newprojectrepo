public class Task_Svc {
    
    public static void process(List<Account> lstAcc){
        Map<Id,List<Task>> mapTasks = processTasks(lstAcc);
        upsertTasks(mapTasks);
    }
    
    public static void upsertTasks(Map<Id,List<Task>> mapTasks){
        List<Task> lstTasks = new List<Task>();
        for(Id id : mapTasks.keyset()){
            lstTasks.addAll(mapTasks.get(id));
        }
        upsert lstTasks;
    }
    
    public static Map<Id,List<Task>> processTasks(List<Account> lstAcc){
        Map<Id,List<Task>> mapTasksPerAccId = retrieveTask(lstAcc);
        Map<Id,Set<String>> mapReasons = Account_Svc.returnReasons(lstAcc);
        
        for(Id accId : mapTasksPerAccId.keyset()){
            Set<String> reasons = mapReasons.get(accId);
            if(reasons==null || reasons.size() == 0) continue;
            
            for(Task task : mapTasksPerAccId.get(accId)){
                if(!reasons.contains(task.Subject)){
                    task.Status = 'Completed';
                    reasons.remove(task.Subject);
                }else if(reasons.contains(task.Subject)){
                    reasons.remove(task.Subject);
                }
            }
            if(reasons.size() > 0){
                for(String str : reasons){
                    mapTasksPerAccId.get(accId).add(new Task(whatId=accId,Subject = str,Status = 'In Progress'));
                }
            }
        }
        return mapTasksPerAccId;
    }
    
    public static Map<Id,List<Task>> retrieveTask(List<Account> lstAcc){
        Map<Id,List<Task>> mapTasksPerAccId = new Map<Id,List<Task>>();
        
        for(Account acc : lstAcc)
            mapTasksPerAccId.put(acc.Id,new List<Task>());
        
        List<Task> tasks = [select Subject,WhatId,Status from task where whatId IN : lstAcc];
        for(Task tsk : tasks){
            if(mapTasksPerAccId.containsKey(tsk.whatId))
                mapTasksPerAccId.get(tsk.whatId).add(tsk);
        }
        return mapTasksPerAccId;
    }
    
}