global class LeadProcessor implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query = 'select id,LeadSource from Lead';
      return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
         for(sobject s : scope){
            s.put('LeadSource','Dreamforce'); 
         }
         update scope;
    }

   global void finish(Database.BatchableContext BC){
   
       
   }

}