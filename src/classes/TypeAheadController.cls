public class TypeAheadController {

    public String searchTerm            { get; set; }
    public String selectedLead          { get; set; }
    public String leadIdsSet            { get; set; }
    public String campaignId            { get; set; }
    
    public TypeAheadController(ApexPages.StandardController controller) {
            campaignId = (String)controller.getId();
    }
    
    @RemoteAction  
    public static List<LeadWrapper> getSearchSuggestions(String searchString){ 
       
       List<LeadWrapper> leadWrappers = new List<LeadWrapper>(); 
       String searchTerm = searchString + '*';
       String query = 'FIND :searchTerm IN ALL FIELDS RETURNING Lead (Id, Name, Company, Email, Phone ) LIMIT 2000';
       List<List<sObject>> searchObjects = Search.query( query );
       if(!searchObjects.isEmpty()){  
            for(List<SObject> objects : searchObjects){  
                 for(SObject obj : objects){
                      if(obj.getSObjectType().getDescribe().getName().equals('Lead')){  
                           Lead l = (Lead)obj;  
                           leadWrappers.add(new LeadWrapper(l.name, l.Id, l.Email, l.Phone));  
                      } 
                 }  
            }  
       } 
        
       return leadWrappers;  
   }
   
   public void Associate() {
   
       List<CampaignMember> campaignMemberList = new List<CampaignMember>();
       
       if( !String.isBlank(leadIdsSet) && leadIdsSet != '' && leadIdsSet != null ) {
       
           List<String> leadIdsList = leadIdsSet.split(',');
           for( String str : leadIdsList ) {
                CampaignMember cm = New CampaignMember(CampaignId=campaignId,LeadId= str.trim() ,Status='Sent' );
                campaignMemberList.add(cm);
           }
           
           if ( !campaignMemberList.isEmpty() ) {
                String errorMessage = '';
                List<Database.saveResult> saveResultList = Database.insert(campaignMemberList, false);
                for ( Database.saveResult sr : saveResultList ) {
                      if ( !sr.isSuccess() ) {
                           List<Database.Error> errors = sr.getErrors();
                           for ( Database.Error error : errors ) {
                                 errorMessage += ''+error.getMessage()+' ';
                           }
                      }
                }
                if ( !String.isBlank(errorMessage) ) {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMessage ));
                } else {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Campaign Member Addess Successfully!' ));
                }
           }
       } else {
           
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select atleast one Lead Record'));
       }
   }
   
    
    public class LeadWrapper {  
    
      public String label              { get; set; }  
      public String value              { get; set; } 
      public String email              { get; set; }
      public String phone              { get; set; } 
      
      public LeadWrapper (String label, String value, String email, String phone){  
           this.label = label;  
           this.value = value;
           this.email = email;
           this.phone = phone;  
      }  
   }
    
}