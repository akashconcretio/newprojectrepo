public class TWCreateUpdateTask{
    public  TWTasksWrapper twTaskWrapper{get;set;}
    public  TWTasksWrapper.TWProjectTasksList twTasksListWrapper{get;set;}
    public  list<SelectOption> lstOfProjects{get;set;}
    public  list<SelectOption> lstOfProjectsTasksList{get;set;}
    public  list<SelectOption> lstOfTasksInTasksList{get;set;}
    public  string selected{get;set;}
    public  string selectedTasksList{get;set;}
    public  string selectedTask{get;set;}
    public integer showhide{get;set;}
    public list<string> lstOfDesiredJsonForAllTasks;
    public string selectedTaskDetails{get;set;}
    public boolean visible{get;set;}
    public string taskToBeUpdated{get;set;}
    public TWCreateUpdateTask(){   //  ApexPages.StandardController controller
        showhide  = 1;
        lstOfProjects = new list<SelectOption>();
        AllTeamworkProjects();
        selectedTaskDetails='';
        visible  = false;
    }
    
    public void AllTeamworkProjects(){  //This method calls all projects in a company. and put them in a picklist.
        showhide  = 1;
        string response;
        HttpRequest req ;
        Http http;
        HTTPResponse res;
        TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
        String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
        req = new HttpRequest(); 
        req.setEndpoint(Configure.Endpoint__c+'projects.json');
        req.setMethod('GET');
        req.setHeader('Accept', 'application/json; charset=UTF-8');
        req.setHeader('Content-Type','application/json');  
        req.setHeader('Authorization', 'Basic '+authorizationHeader);
        req.setTimeout(120000);
        http = new Http();
        res = http.send(req);
        response = string.valueof(res.getBody()).replaceAll('-','_');
        twTaskWrapper = TWTasksWrapper.parse(response);
        //system.debug('resp:::'+response);
        lstOfProjects.add(new SelectOption('None','Select Project'));
        for(TWTasksWrapper.cls_projects tsk : twTaskWrapper.projects){
            lstOfProjects.add(new SelectOption(tsk.id,tsk.name));
        }
    }
    public void ProjectsAllTaskLists(){      //This method calls all taskslist under a given project in a company. and put them in a picklist.
        showhide  = 2;
        lstOfProjectsTasksList = new list<SelectOption>();
        string response;
        HttpRequest req ;
        Http http;
        HTTPResponse res;
        TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
        String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
        req = new HttpRequest(); 
        req.setEndpoint(Configure.Endpoint__c+'projects/'+selected+'/tasklists.json');
        //system.debug('TList::'+req.getEndpoint());
        req.setMethod('GET');
        req.setHeader('Accept', 'application/json; charset=UTF-8');
        req.setHeader('Content-Type','application/json');  
        req.setHeader('Authorization', 'Basic '+authorizationHeader);
        req.setTimeout(120000);
        http = new Http();
        res = http.send(req);
        response = string.valueof(res.getBody()).replaceAll('-','_').replaceAll('private','privateList');
        twTaskWrapper = TWTasksWrapper.parseTasksList(response);
        lstOfProjectsTasksList.add(new SelectOption('None','Select TaskList'));
        //system.debug('TList::'+twTaskWrapper.tasklists[0].name);
        if(twTaskWrapper.tasklists.size()!=0){
            for(TWTasksWrapper.cls_tasklists taskList : twTaskWrapper.tasklists){
                if(selected!='None'){
                    lstOfProjectsTasksList.add(new SelectOption(taskList.id,taskList.name));
                } else {
                    showhide=1;
                }
            
            }
        }
        
    }
    public void AllTasksInTasksList(){   // This method calls all tasks under a given taskslist in a company. and put them in a picklist.
        lstOfTasksInTasksList = new list<SelectOption>(); 
        lstOfDesiredJsonForAllTasks = new list<string>();
        //selectedTaskDetails = new list<TWTasksWrapper.cls_todo_items>();
        showhide  = 3;
        
        string response = '';
        HttpRequest req ;
        Http http;
        HTTPResponse res;
        TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
        String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
        req = new HttpRequest(); 
        req.setEndpoint(Configure.Endpoint__c+'tasklists/'+selectedTasksList+'/tasks.json');
        system.debug('TList::'+req.getEndpoint());
        req.setMethod('GET');
        req.setHeader('Accept', 'application/json; charset=UTF-8');
        req.setHeader('Content-Type','application/json');  
        req.setHeader('Authorization', 'Basic '+authorizationHeader);
        req.setTimeout(120000);
        http = new Http();
        res = http.send(req);
        list<string> lstAllTasksInTL = string.valueof(res.getBody()).replaceAll('-','_').replaceAll('private','privateList').split(',"DLM":');  //A List That contains Error Free Json for parsing..
        for(integer i=1;i<=lstAllTasksInTL.size()-1;i++){
           lstOfDesiredJsonForAllTasks.add(lstAllTasksInTL[i].substring(13,lstAllTasksInTL[i].length()));   // ----------++++++++>>>>>  A list that does't contain DLM Variable in json that causes a problem for parser.
        }
        response = lstAllTasksInTL[0]+string.join(lstOfDesiredJsonForAllTasks,'');
        system.debug('response::++'+response);
        twTaskWrapper = TWTasksWrapper.parseTasksINTasksList(response);      //-----------------++++++++++  Parsing In The Wrapper Class...
        lstOfTasksInTasksList.add(new SelectOption('None','Select Task'));
        for(TWTasksWrapper.cls_todo_items tsk : twTaskWrapper.todo_items){
            lstOfTasksInTasksList.add(new SelectOption(string.valueof(tsk.id),tsk.content));
            // if(selectedTask.equals(string.valueof(tsk.id))){
            //     selectedTaskDetails = tsk.content;
            //     system.debug('STS:::'+selectedTaskDetails);
            // }
        }
        system.debug('STS:::'+selectedTask); 
    }
    public void ShowSelectedTaskNameAndID(){
        showhide  = 4;
        for(TWTasksWrapper.cls_todo_items tsk : twTaskWrapper.todo_items){
            if(selectedTask.equals(string.valueof(tsk.id))){
                selectedTaskDetails = tsk.content;
                system.debug('STS:::'+selectedTaskDetails);
                Break;
            }
        }
    }
    public void UpdateTask(){
        showhide  = 5;
        if(taskToBeUpdated!=null){
            HttpRequest req ;
            Http http;
            HTTPResponse res;
            TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
            String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
            req = new HttpRequest(); 
            req.setEndpoint(Configure.Endpoint__c+'tasks/'+selectedTask+'.json');
            system.debug('TList::'+req.getEndpoint());
            req.setMethod('PUT');
            req.setHeader('Accept', 'application/json; charset=UTF-8');
            req.setHeader('Content-Type','application/json');  
            req.setHeader('Authorization', 'Basic '+authorizationHeader);
            string taskJsonBody = '{"todo-item":{"content":"'+taskToBeUpdated+'","notify":false,"description":"","due-date":"20160712","start-date":"20160706","private":"0","grant-access-to":"","priority":"high","progress":"20","estimated-minutes":"0","responsible-party-id":"197210","attachments":{},"tasklistId":"","pendingFileAttachments":"","tags":"api,documentation"}}';
            req.setBody(taskJsonBody);
            req.setTimeout(120000);
            http = new Http();
            res = http.send(req);
            system.debug('resBody:::'+res.getBody());
        }
    }
    public void AllUsers(){
        showhide  = 5;
        string response = '';
        HttpRequest req ;
        Http http;
        HTTPResponse res;
        TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
        String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
        req = new HttpRequest(); 
        req.setEndpoint(Configure.Endpoint__c+'companies/'+Configure.Company_Id__c+'/people.json');
        //system.debug('TList::'+req.getEndpoint());
        req.setMethod('GET');
        req.setHeader('Accept', 'application/json; charset=UTF-8');
        req.setHeader('Content-Type','application/json');  
        req.setHeader('Authorization', 'Basic '+authorizationHeader);
        req.setTimeout(120000);
        http = new Http();
        res = http.send(req);
        system.debug('res::++'+res.getBody());
        response  = string.valueof(res.getBody()).replaceAll('-','_');
        twTaskWrapper = TWTasksWrapper.parseAllUsers(response); 
    }
}