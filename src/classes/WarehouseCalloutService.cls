public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    // complete this method to make the callout (using @future) to the
    // REST endpoint and update equipment on hand.
    @future(callout=true)
    public static void runWarehouseEquipmentSync(){
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(WAREHOUSE_URL);
        req.setMethod('GET');
        req.setHeader('Accept','application/json');
        req.setHeader('content-type','application/json');
        res = http.send(req);
        product pd = product.parse('{"Equipment":'+res.getBody().replace('_id','id')+'}');
        // map<string,object> mapProducts = (map<string,object>)JSON.deserializeUntyped(res.getBody());
        List<Product2> lstProduct = new list<Product2>();
        system.debug('test :: '+pd.Equipment[0]);
        
        for(product.cls_Equipment pdt : pd.Equipment){
            Product2 p2 = new Product2();
            p2.Name = pdt.name;
            p2.IsActive = true;
            p2.Cost__c = pdt.cost;
            p2.Current_Inventory__c = pdt.quantity;
            p2.Lifespan_Months__c = pdt.lifespan;
            p2.Maintenance_Cycle__c = pdt.maintenanceperiod;
            p2.Replacement_Part__c = true;
            p2.Warehouse_SKU__c = pdt.sku;
            
            lstProduct.add(p2);
        }
        if(lstProduct.size()>0){
            upsert lstProduct Warehouse_SKU__c;
        }
    }

}