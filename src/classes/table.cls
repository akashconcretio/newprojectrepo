public class table { 
     public  integer numbr{get;set;}
     public  integer factr{get;set;}
    public List<tablewrapper> lstTable{get;set;}
    public  boolean showhide{get;set;}
    
    public table(){
        showhide = false;
        // factr = 5;
    }
    
    public void calculate(){
        // system.debug('Akash::'+true);
        // system.debug('num::'+numbr);
        // system.debug('fac::'+factr);
        
        if(numbr>0 && factr>0){
            integer i;
            // showhide = true;
            lstTable = new List<tablewrapper>();
            for(i=1;i<=factr;i++){
                lstTable.add(new tablewrapper(numbr*i,i,numbr));
            }
            // return tableBody;
        }else{
            
            showhide = false;
            // return null;
        }
        
    }
    public class tablewrapper{
        public integer result{get;set;}
        public integer factor{get;set;}
        public integer numbrT{get;set;}
        public tablewrapper(integer a,integer b,integer c){
            result  =a;
            factor = b;
            numbrT = c;
        }
    }
}