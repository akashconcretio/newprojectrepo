@isTest
private class ProjectRESTServiceTest {
	private static testMethod void test() {
	    
	    Opportunity opp = new Opportunity();
	    opp.Name = 'opp';
	    opp.CloseDate = Date.newInstance(2018,01,20);
	    opp.stageName = 'prospecting';
	    insert opp;
	    
	    
	    RestRequest req = new RestRequest(); 
	    req.requestBody = blob.valueof('{"ProjectRef":"65hgg","ProjectName":"prj123","OpportunityId":"0060K00000RLIrl","StartDate":"2018-01-19","EndDate":"2018-01-20","Amount":100,"Status":"Billable"}');
        RestResponse res = new RestResponse();
        
        req.requestURI = 'https://ap6.salesforce.com/services/apexrest/project';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        Date newDate = Date.newInstance(2018,01,19);
        Date endDate = Date.newInstance(2018,01,20);
        Double amm = 100;
        //string headerString = '';
        
        // RestContext.request.addHeader();
        ProjectRESTService.postProjectData('65hgg','prj123',opp.Id,Date.newInstance(2018,01,19),Date.newInstance(2018,01,20),100,'Billable');
        ProjectRESTService.postProjectData('65hgg','prj123','0060K00000RLIrg',Date.newInstance(2018,01,19),Date.newInstance(2018,01,20),100,'Billable');
	}

}