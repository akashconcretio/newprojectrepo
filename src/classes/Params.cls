public class Params {
    public string value1{get;set;}
    public string value2{get;set;}
    public string value3{get;set;}
    public string dateValue{get;set;}
    public Params(){
        dateValue = string.valueof(system.today().adddays(-7));
        // dateValue = '2017-01-13';
    }
    
    public void showValues(){
        if(!string.isEmpty(value1)){
            system.debug('Hello'+Apexpages.currentPage().getParameters().get('vv1'));
        system.debug('value1::'+value1);
        system.debug('value2::'+value2);
        system.debug('value3::'+value3);
        system.debug('dateValue::'+dateValue);
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please enter 1st value'));
        }
    }
}