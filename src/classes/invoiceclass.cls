public class invoiceclass{
 
 public list<Invoice_line_Item__c> invoice{get;set;}
 public string invoiceid{get;set;}
 
 public invoiceclass()
     {
       invoiceid= ApexPages.currentPage().getParameters().get('id');
       showlineiten();
     }
 
 public void showlineiten()
   {
       invoice=[select Name,Quantity__c,invoice__r.Account__r.BillingStreet,invoice__r.Name,Price__c,total__c from Invoice_line_Item__c];

   }
}