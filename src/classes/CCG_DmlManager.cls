/**
* @author Badan Singh Pundeer
* @date 29.05.2018
*
* @group CBM
*
* @description Performs DML operation for filter fields && columns Data.
*
* @history
* version                           | author                                    | changes
*=============================================================================
* 1.0 (29.05.2018)          | Badan Pundeer                   | initial version
*/
public class CCG_DmlManager {
    public CCG_DmlManager(){
        
    }
     /**
    * @description here we are saving filter fields , selected from multi-select picklist per object.
    * @param filterFields, Filter Fields Per Object in JSON form.
    */
    @RemoteAction
    public static void saveFilterFields(String filterFields){
        map<string,List<string>> mapFilterFields =  new map<string,List<string>>();
        CCG_FilterFieldWrapper wrp = CCG_FilterFieldWrapper.parse(filterFields);
        
        for(CCG_FilterFieldWrapper.cls_setFilterFields ff:wrp.setFilterFields){
            mapFilterFields.put(ff.objName,new List<string>());
            for(CCG_FilterFieldWrapper.cls_fields ffields : ff.fields){
                mapFilterFields.get(ff.objName).add(ffields.fieldApiName);
            }
        }
        List<Spender_Selektion_Object__c> lstSelObjs = new List<Spender_Selektion_Object__c>();
        // List<SpenderSelection_Filter_FIeld__c> lstOfspendersectionFilterFieldsInsert = new List<SpenderSelection_Filter_FIeld__c>();
        // List<SpenderSelection_Filter_FIeld__c> lstOfspendersectionFilterFieldsUpdate = new List<SpenderSelection_Filter_FIeld__c>();
        
        for(string str : mapFilterFields.keyset()){
            // SpenderSelection_Filter_FIeld__c spenderSelektion = new SpenderSelection_Filter_FIeld__c();
            // if(SpenderSelection_Filter_FIeld__c.getInstance(str)!=null){
            //     SpenderSelection_Filter_FIeld__c spenderSelection = SpenderSelection_Filter_FIeld__c.getInstance(str);
            //     if(!mapFilterFields.get(str).isEmpty()){
            //         spenderSelection.Filter_Fields1__c = string.join(mapFilterFields.get(str),',').length()>255?string.join(mapFilterFields.get(str),',').subString(0,254):string.join(mapFilterFields.get(str),',');
            //         if(string.join(mapFilterFields.get(str),',').length()>255){
            //             spenderSelection.Filter_Fields2__c = string.join(mapFilterFields.get(str),',').subString(254,string.join(mapFilterFields.get(str),',').length());
            //         }
            //     }else{
            //         spenderSelection.Filter_Fields1__c = '';
            //         spenderSelection.Filter_Fields2__c  = '';
            //     }
            //     lstOfspendersectionFilterFieldsUpdate.add(spenderSelection);
            // }else{
            //     spenderSelektion.Name = str;
            //     if(!mapFilterFields.get(str).isEmpty()){
            //         spenderSelektion.Filter_Fields1__c = string.join(mapFilterFields.get(str),',').length()>255?string.join(mapFilterFields.get(str),',').subString(0,254):string.join(mapFilterFields.get(str),',');
            //         if(string.join(mapFilterFields.get(str),',').length()>255){
            //             spenderSelektion.Filter_Fields2__c = string.join(mapFilterFields.get(str),',').subString(254,string.join(mapFilterFields.get(str),',').length());
            //         }
            //     }else{
            //         spenderSelektion.Filter_Fields1__c = '';
            //         spenderSelektion.Filter_Fields2__c  = '';
            //     }
            //     lstOfspendersectionFilterFieldsInsert.add(spenderSelektion);
            // }
            
            Spender_Selektion_Object__c selObj = new Spender_Selektion_Object__c();
            selObj.Name = str;
            selObj.Object_Name__c = str;
            selObj.Filter_Fields__c = string.join(mapFilterFields.get(str),',');
            lstSelObjs.add(selObj);
            upsert lstSelObjs Object_Name__c;
            
        }
        // if(lstOfspendersectionFilterFieldsUpdate!=null){
        //     update lstOfspendersectionFilterFieldsUpdate; 
        // }
        // if(lstOfspendersectionFilterFieldsInsert!=null){
        //     insert lstOfspendersectionFilterFieldsInsert;
        // }
    }
    
     /**
    * @description here we are saving Columns data for export table Columns in CS.
    * @param columns, Columns data per row in JSON form.
    */
    @RemoteAction
    public static void saveColumns(String columns){
        List<Spender_Selektion_Columns_Setting__c> columnSettingInsert = new List<Spender_Selektion_Columns_Setting__c>();
        map<string,Spender_Selektion_Columns_Setting__c> mapOfAllCoumnSettingRecord = Spender_Selektion_Columns_Setting__c.getAll();
        
        CCG_ColumnDetailWrpr col = CCG_ColumnDetailWrpr.parse(columns);
        if(col!=null){
            for(CCG_ColumnDetailWrpr.cls_setColumns colName : col.setColumns){
                    Spender_Selektion_Columns_Setting__c colSet = new Spender_Selektion_Columns_Setting__c();
                    colSet.Name = colName.Column;
                    colSet.Account_Field__c = colName.AccountField;
                    colSet.Contact_Field__c = colName.ContactField;
                    colSet.Fixed__c = Boolean.valueof(colName.isFixed);
                    colSet.Lead_Fields__c = colName.LeadField;
                    columnSettingInsert.add(colSet);
            }
        }
        if(!mapOfAllCoumnSettingRecord.isEmpty()){
            delete mapOfAllCoumnSettingRecord.values();
        }
        if(!columnSettingInsert.isEmpty()){
            insert columnSettingInsert;
        }
    }
}