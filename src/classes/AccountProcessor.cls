public class AccountProcessor {
    
    @Future
    Public Static void countContacts(List<Id> ids){
        List<Contact> lstCon = [select id,Name,AccountId from Contact where AccountId=:ids];
        Map<Id,Integer> mapCount  = new Map<Id,Integer>();
        for(Contact con : lstCon){
            if(mapCount.containskey(con.AccountId)){
                mapCount.put(con.AccountId,mapCount.get(con.AccountId)+1);
            }else{
                mapCount.put(con.AccountId,1);
            }
        }
        List<Account> lstAcc = [select id,Number_of_Contacts__c from Account where Id=:ids];
        for(Account acc : lstAcc){
            acc.Number_of_Contacts__c = mapCount.get(acc.Id);
        }
        if(lstAcc!=null){
            update lstAcc;
        }
    }
    
}