@isTest
private class feecountonAccountTest {
	private static testMethod void testFeeCount() {
	    Account acc = new Account();
	    acc.name = 'Akash';
	    insert acc;
	    contact con = new contact();
	    con.Accountid = acc.id;
	    con.lastname = 'Test';
	    con.fee__c = 900;
	    insert con;
	    Account acc1 = [select total_fee__c from Account where name = 'Akash' limit 1];
	    system.assertEquals(900,acc1.total_fee__c);
	    
	    contact con1 = new contact();
	    con1.Accountid = acc.id;
	    con1.lastname = 'Test1';
	    con1.fee__c = 800;
	    insert con1;
	    Account acc2 = [select total_fee__c from Account where name = 'Akash' limit 1];
	    system.assertEquals(1700,acc2.total_fee__c);
	    
	    delete con;
	    Account acc3 = [select total_fee__c from Account where name = 'Akash' limit 1];
	    system.assertEquals(800,acc3.total_fee__c);
	}
}