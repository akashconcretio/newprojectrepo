public class AddContactsCtrl 
{ 
    public list<Contact> conList{ get; set; }
    public String accId;
    public Integer count{ get; set; }

// 
    public AddContactsCtrl(ApexPages.StandardController controller) {
        count = 0;
        accId = ApexPages.currentPage().getParameters().get('id');
        conList=new list<Contact>();
    }

    
     
Public PageReference save(){
    try{
		insert conList;
		PageReference page = new PageReference('/'+ accId);
    	page.setRedirect(true);
    	return page;
	}catch(DmlException e){
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
	}
	return null;
}


Public void addAccount()
{
  if(count > 0){
       conList=new list<Contact>();
		for(Integer i =0; i< count; i++ ){
			Contact con = new Contact();
			con.LastName = 'Test';
			con.AccountId = accId;
			conList.add(con);			
		}
	} 
}

}