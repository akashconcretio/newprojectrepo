global class EmailSentUsingEmailTemplate implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
      string query = 'select id ,Name,Industry ,createdDate from Account where Industry=null AND CreatedDate = today';
      return Database.getQueryLocator(query);
   }
   global void execute(Database.BatchableContext BC, List<Account> scope){
      system.debug('scope:::'+scope);
       list<contact> lstcon = new list<contact>();
       list<Messaging.SingleEmailMessage> listOfEmails = new list<Messaging.SingleEmailMessage>();
      if(scope.size()>0){
          EmailTemplate templateId = [Select id,Name from EmailTemplate where name = 'AccountIndustryNotify'];
              for(Account mailToBeSent : scope){
                  contact con = new contact(LastName=mailToBeSent.Name,Email='akashpundir786@gmail.com');
                    lstcon.add(con);
              }
              insert lstcon;
              for(contact cont : [select id,LastName,Email from contact where lastName =: lstcon[0].LastName]){
                  Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    email.setSaveAsActivity(false);
                    email.setTargetObjectId(cont.Id);
                    email.setTemplateId(templateId.Id);
                    listOfEmails.add(email);
              }
                system.debug('listOfEmails:::'+listOfEmails);
      }
      if(listOfEmails.size()>0){
          Messaging.sendEmail(listOfEmails);
      }
   }
   global void finish(Database.BatchableContext BC){
   }
}