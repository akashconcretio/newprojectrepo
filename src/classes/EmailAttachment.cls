public class EmailAttachment {
    public list<Account> lstAcc{get;set;}
     
    public static string accId{get;set;}
    
    public EmailAttachment(ApexPages.StandardController controller){
        accId = ApexPages.currentPage().getParameters().get('id');
        AccDetails();
        // if(ApexPages.currentPage().getParameters().get('send')==null){
        //     Sendemail();
        // }
    } 
    public void AccDetails(){
        lstAcc = new list<Account>();
        lstAcc = [select Id,Name,Phone,Email__c,Type,createdDate from Account where ID =: accId];
        // system.debug('lstAcc+++'+lstAcc);
         //return lstAcc;
    }
    // public list<Account> getDesiredAccDetails(){
        
    //     return getAccDetails(accId);
    // }
    public static void Sendemail(){
        // try{
        if(ApexPages.currentPage().getParameters().get('send') != 'true'){
            string selectedAccId = accId;
            /*string selectedAccId = '0012800001DFJjA';*/
            system.debug('selectedAccId+++'+selectedAccId);
            list<string> emails = new list<string>{'akash@concret.io'};
            PageReference pdf = Page.EmailAttachment;
             pdf.getParameters().put('id',selectedAccId);
             pdf.getParameters().put('send','true');
            Blob b = pdf.getContent();
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('attachment.pdf');
            efa.setBody(b);
             system.debug('efa+++'+efa.getBody());
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
    
            email.setSubject( 'Account Information');
            email.setplaintextbody('test');
            email.setToAddresses(emails);
            try{
                Messaging.SendEmailResult [] r = 
    			Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
            }catch(Exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,e.getmessage()));
            }
         }
    }
}