public with sharing class WithsharingExample {
    public static List<Account> getAcc(){
		List<Account> acc = [select id ,name from Account];
		system.debug(acc.size()+' : '+'acc :: '+acc); 
        return acc;
    }
}