public class updateTaskEvent{
    public static String updatingActivity(Opportunity opp){
        String priority = '4';
        if(opp.Stagename != '1 Month' && opp.Stagename != '3 Month' && opp.Stagename != '6 Month'){
            return priority;
        }else{ 
           if(opp.Opportunity_Size__c == '> 25M' && (opp.Stagename=='1 Month' || opp.Stagename=='3 Month' || opp.Stagename=='6 Month')){
                priority = '1';
            }else if(opp.Opportunity_Size__c == '< 25M' && (opp.Stagename=='1 Month')){
                priority = '2';
            }else if(opp.Opportunity_Size__c == '< 25M' && (opp.Stagename=='6 Month')){
                priority= '3';
            }else{
                priority= '4'; 
            } 
        }
        return priority ;
    }
}