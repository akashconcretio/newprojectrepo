public class Weather{
    public Weather() {
        newWeatherReport = new weather__c();
        newWeatherReport.City_name__c = ApexPages.currentPage().getParameters().get('city');
        enterdate = ApexPages.currentPage().getParameters().get('date');
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.worldweatheronline.com/premium/v1/past-weather.ashx?key=0b2d12a95a484991a2d92416170907&q='+String.valueOf(newWeatherReport.City_name__c)+'&date='+enterdate+'&format=json');
        req.setMethod('GET');
        req.setHeader('Content-Type','application/json; charset=UTF-8');  
        req.setTimeout(120000);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        wratherData = WeatherModel.parse(res.getBody());
        
    }
   // public String cityname {get;set;}
    public string  enterdate {get;set;} 
    //public string currentweatherTime{get;set;}
    public WeatherModel  wratherData{get;set;}
    public weather__c newWeatherReport{get;set;}
    public Weather_History__c weatherHistory{get;set;}
    public string modifiedJSON;
    public string resBody;
    
        
    public Weather(ApexPages.StandardController controller) { 
        newWeatherReport = new weather__c();
    }
    public  void showWeatherDetail(){
        HttpRequest req = new HttpRequest();
        enterdate = String.valueOf(newWeatherReport.Date__c);
         
        req.setEndpoint('https://api.worldweatheronline.com/premium/v1/past-weather.ashx?key=0b2d12a95a484991a2d92416170907&q='+String.valueOf(newWeatherReport.City_name__c)+'&date='+enterdate+'&format=json');
        req.setMethod('GET');
        req.setHeader('Content-Type','application/json; charset=UTF-8');  
        req.setTimeout(120000);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        resBody = string.valueof(res.getBody());
        modifiedJSON = resBody.replaceAll('time', 'weathertime');
        system.debug('value::::'+modifiedJSON);
        wratherData = WeatherModel.parse(modifiedJSON);
        
    }
    public void save(){
         
        insert newWeatherReport;
        
        weatherHistory = new Weather_History__c();
        weatherHistory.weather__c = newWeatherReport.id;
        
            weatherHistory.Date__c = newWeatherReport.date__c;
        
        weatherHistory.city__c = wratherData.data.request[0].query;
        weatherHistory.Maximum_Temprature__c = wratherData.data.weather[0].maxtempC;
        weatherHistory.Current_Tempreture__c = wratherData.data.weather[0].hourly[0].tempC;
        weatherHistory.sunrise__c = wratherData.data.weather[0].astronomy[0].sunrise;
        weatherHistory.Humidity__c = wratherData.data.weather[0].hourly[0].humidity;
        weatherHistory.Wind_Speed__c = wratherData.data.weather[0].hourly[0].windspeedKmph;
        insert weatherHistory;
    }
    // public void showWeatherHistory(){
    //      todayWeatherHistory = [select city__c,Maximum_Temprature__c,Current_Tempreture__c,sunrise__c,Humidity__c,Wind_Speed__c from Weather_History__c where createddate = today];
    // }
    public pagereference generatePDF(){
        //pagereference pageref = page.generateweatherpdf; 
        pagereference pageref = new pagereference('/apex/generateweatherpdf?city='+newWeatherReport.City_name__c+'&date='+enterdate+'');
        pageref.setredirect(true);
        return pageref;
    }
}