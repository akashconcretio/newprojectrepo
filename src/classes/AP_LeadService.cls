/*
    @Author :: Akash Kumar || 13 Nov 2019
    @Description :: 
*/
public class AP_LeadService{
   
   
   /*
        @Description :: This method is being called from a process builder named "SyncAutoPilotToUpdateLead"
        @param :: processBuilderInput :: Its a wrapper class which gets input from process builder like lead email and lead Id . 
    
    */
    @InvocableMethod
    public static void updateLeadFirstLandingUrl(List<processBuilderInputs> processBuilderInput) {
        retrieveLeadFirstLandingUrlInfo(processBuilderInput[0].leadEmail,processBuilderInput[0].leadId);
    }
    
    /*
        @Description :: 
                        we are calling this method in future because method invoked from Process builder does't allow API call in same transaction .
                        
        @param :: urlPath :: Lead's Email
                  leadId  :: Lead's Id
    */
    @future(callout = true)
    public static void retrieveLeadFirstLandingUrlInfo(String leadEmail,String leadId){
        AP_IntegrationService.retrieveLeadFirstLandingUrlInfo(new lead(id=leadId,Email=leadEmail));
    }
    
    // Its a wrapper class which gets input from process builder ("SyncAutoPilotToUpdateLead") like lead email and lead Id . 
    public class processBuilderInputs{
        @InvocableVariable
        public String leadEmail;
        
        @InvocableVariable
        public String leadId;
    }
}