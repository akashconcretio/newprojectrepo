public class AddPrimaryContact implements Queueable {
    
    private Contact Contacts;
    private String stateAddress;
    
    public AddPrimaryContact(Contact records, String state) {
        this.Contacts = records;
        this.stateAddress = state;
    }

    public void execute(QueueableContext context) {
        List<Account> lstAcc = [select id,Name,BillingState from Account Where BillingState = : stateAddress limit 200];
        List<Contact> lstContact = new list<Contact>();
        for(Account acc : lstAcc){
            Contact con = this.Contacts;
            con.LastName  = acc.Name;
            con.MailingState = acc.BillingState;
            con.AccountId = acc.Id;
            lstContact.add(con);
        }
        insert lstContact;
        // for (Account account : accounts) {
        //   account.parentId = parent;
        //   // perform other processing or callout
        // }
        // update accounts;
    }
    
}