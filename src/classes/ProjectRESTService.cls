@RestResource(urlMapping='/project/*')
    global class ProjectRESTService {
        
      @HttpPost
      global static String postProjectData(String ProjectRef, String ProjectName, String OpportunityId, Date StartDate, Date EndDate, Double Amount, String Status){
        // RestRequest req = RestContext.request;
        // RestResponse res = Restcontext.response;
        // system.debug('res : '+RestContext.request.params.get('ProjectName'));
        
        //Map<String,Object> mapParse = (Map<String,Object>)JSON.deserializeUntyped(req.requestBody.toString());
        
        
        Project__c proj = new Project__c();
        // proj.Name = String.valueof(mapParse.get('ProjectName'));
        // proj.ProjectRef__c = String.valueof(mapParse.get('ProjectRef'));
        //  proj.Opportunity__c = String.valueof(mapParse.get('OpportunityId'));
        //   proj.Start_Date__c = Date.valueof(String.valueof(mapParse.get('StartDate')));
        //   proj.End_Date__c = Date.valueof(String.valueof(mapParse.get('EndDate')));
        //     proj.Billable_Amount__c = Decimal.valueOf(String.valueof(mapParse.get('Amount')));
        //     proj.Status__c = String.valueof(mapParse.get('Status'));
        
        // proj.Name = RestContext.request.params.get('ProjectName');
        // proj.ProjectRef__c = RestContext.request.params.get('ProjectRef');
        //  proj.Opportunity__c = RestContext.request.params.get('OpportunityId');
        //   proj.Start_Date__c = Date.valueOf(RestContext.request.params.get('StartDate'));
        //   proj.End_Date__c = Date.valueOf(RestContext.request.params.get('EndDate'));
        //     proj.Billable_Amount__c = Double.valueOf(RestContext.request.params.get('Amount'));
        //     proj.Status__c = RestContext.request.params.get('Status');
        
        proj.Name = ProjectName;
        proj.ProjectRef__c = ProjectRef;
         proj.Opportunity__c = OpportunityId;
          proj.Start_Date__c = StartDate;
           proj.End_Date__c = EndDate;
            proj.Billable_Amount__c = Amount;
            proj.Status__c = Status;
            
            
                Savepoint sp = Database.setSavepoint();
            try{
                
                upsert proj ProjectRef__c;
                List<Opportunity> lstOpp = [select Id,DeliveryInstallationStatus__c from opportunity where id=:proj.Opportunity__c];
                if(lstOpp!=null && lstOpp.size()>0){
                    lstOpp[0].DeliveryInstallationStatus__c = 'In Progress';
                }
                update lstOpp;
                return 'OK';
            }catch(Exception ee){
                Database.RollBack(sp);
                return ee.getMessage();
            } 
        
        // Id accId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        // list<contact> lstcontact =[Select id , name,Phone,Fax,Email from contact where Accountid=:accId ];
       
      }
    }