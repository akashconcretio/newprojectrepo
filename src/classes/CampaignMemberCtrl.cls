public class CampaignMemberCtrl {
    public Id campaignId {get;set;}
    public CampaignMemberCtrl(ApexPages.StandardController controller){
        campaignId = Apexpages.currentPage().getParameters().get('Id');         
    }
    @RemoteAction
    public static List<Contact> searchLeadResults(String searchKeyword,List<String> excludeLeadIds,String campaignId) {
        searchKeyword = '%'+searchKeyword+'%';
        
        return [select id,Name from Contact where Name LIKE : searchKeyword /*AND Id NOT IN (select leadId from CampaignMember where CampaignId=:campaignId)*/ limit 20];
    }
}