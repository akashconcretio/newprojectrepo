global class testB implements Database.Batchable<sObject>,Database.stateful {
    integer i=0;
    global Database.QueryLocator start(Database.BatchableContext bc) { 
         return Database.getQueryLocator('select id,Name from Account');
    }
    global void execute(Database.BatchableContext bc, List<sObject> records){
        i++;
        test();
    }    
    global void test(){
        system.debug('test :: '+i);
        i++;
    }
    
    global void finish(Database.BatchableContext bc){
       system.debug('testF :: '+i);
    }    
}