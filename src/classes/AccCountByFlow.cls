global class AccCountByFlow {
	@InvocableMethod//(label='Contact Record')
    global static List<giveOutPutToFlow> getContactList(List<takeInoutFromFlow> requests) {
		string rat = requests[0].ratingValue;
        List<Account> lstAcc = [select id from Account where Rating =:rat];
        return new List<giveOutPutToFlow>{new giveOutPutToFlow(lstAcc.size())};
    }
    
    global class takeInoutFromFlow{
        @InvocableVariable
        global string ratingValue;
    }
    
    global class giveOutPutToFlow{
        @InvocableVariable
        global Integer count;
        global giveOutPutToFlow(Integer count){
            this.count = count;
        }
    }
}