public class RunOnce {
    public static Boolean RunOnce = true;
    public static Map<String,List<Trigger_Handler__c>> mapOFHandlersPerAction; 
    public static List<Sobject> newList;
    public static List<Sobject> oldList;
    public static void RunOnce(){
        RunOnce = false;
        mapOFHandlersPerAction = new Map<String,List<Trigger_Handler__c>>();
        newList  = new List<Sobject>();
        
        oldList = new List<Sobject>();
    }
}