public class triggerToUpdateSheetOnAccount{
    Set<ID> AccountIds;
    public void onAfterInsert(list<Contact>triggerNew){
        AccountIds = new Set<ID>();
        for(contact objCon : triggerNew){
            if(objCon.AccountID != null && objCon.Status__c == 'Active' && objCon.Role__c.Contains('MM Holder')){
                 AccountIds.add(objCon.AccountID);   
            }
        }
        if(AccountIds.size() >0){
            updateAccount(AccountIds);
        }
    }
    public void onAfterUpdate(list<Contact>triggerNew, Map<Id,Contact> triggerOldMap){
        AccountIds = new Set<ID>();
        for(Contact objCon : triggerNew){
            if( (triggerOldMap.get(objCon.ID).Status__c != objCon.Status__c) || (triggerOldMap.get(objCon.ID).Role__c != objCon.Role__c)) 
            {
                AccountIds.add(objCon.AccountID); 
            }
            if(triggerOldMap.get(objCon.ID).AccountID != objCon.AccountID){
                if(objCon.AccountID != Null){
                    AccountIds.add(objCon.AccountID); 
                }
                AccountIds.add(triggerOldMap.get(objCon.ID).AccountID);
            }
            
            
        }
        if(AccountIds.size() >0){
            updateAccount(AccountIds);
        }
        
    }
    public void onAfterDelete(list<Contact>triggerOld){
        AccountIds = new Set<ID>();
        for(Contact objCon : triggerOld){
            AccountIds.add(objCon.AccountID);
        }
        if(AccountIds.size() >0){
            updateAccount(AccountIds);
        }
    }
    
    public void updateAccount(Set<ID> AccIds){
        List<Account> AccountList = [Select Id,Total_Active_Sheet__c,(Select Id,Role__c,Status__c From Contacts ) From Account Where Id IN:AccIds];
        for(Account acc :AccountList){
            //acc.Total_Active_Sheet__c = acc.Contacts.size();
            integer counter = 0;
            for(Contact con :acc.Contacts){             
                if(con.Status__c == 'Active' && con.Role__c != '' && con.Role__c != Null && con.Role__c.Contains('MM Holder')){
                    counter++;
                }
            }
            acc.Total_Active_Sheet__c = counter;
        
        }
        update AccountList;
    }
}