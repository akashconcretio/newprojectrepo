public class RetrieveAllUsers {
	
    @AuraEnabled
    public static List<User> returnUsers(String searchedKeyWord){
        searchedKeyWord = '%'+searchedKeyWord+'%' ;
        return [select name,Id from User where Name like :searchedKeyWord  limit 1000];
    }
}