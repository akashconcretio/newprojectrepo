global class JsPieChart {
    
    public List<PickListWrapper> lstOfPickListValues{get;set;}
    public List<SelectOption> lstAllPickList{get;set;}
    public String jsonData{get;set;}
    // public String selected{get;set;} 
    global JsPieChart(){
        lstAllPickList = new List<SelectOption>();
        // jsonData= '';
        // selected ='Industry';
        lstOfPickListValues = new List<PickListWrapper>();
        lstOfPickListValues = returnAllPickListField();
        
        // lstAcc = [select id,Name,Industry from Account where Industry != null];
        // Map<string,integer> mp = accCountByIndustry(lstAcc);
        // for(string str : mp.keySet()){
        //     jsonData=jsonData+'{ "Label":"'+str+'", "Data":'+mp.get(str)+' },';
        // }
        // if(jsonData!=''){
        //     jsonData = jsonData.removeEnd(',');
        //     jsonData='['+jsonData+']';
        // }
        // system.debug('jsonData:::'+jsonData);
        // jsonData = '[{ "Label":"Delhi", "Data": 12345 },{ "Label":"Gurgao", "Data": 57766 },{ "Label":"Meerut", "Data": 98674 }]';
    }
    @RemoteAction
    public static String recordCountForChart(String selectedPickListValue){
        // ControllingData_Hlpr setupFieldData = new ControllingData_Hlpr();
        // setupFieldData = Utility.fillMappingValue();
        Map<String,Integer> mapCount = new Map<String,Integer>();
        // List<string> lstOfIds = jsonStrig.split(',');
        String query = 'SELECT Id ,'+selectedPickListValue+' FROM Account';
        List<SObject> p = Database.query(query);
        Integer count = 0;
        String jsonData = '';
        system.debug('query::'+query); 
        for(sobject sb : p){
            // Map<string,object> sb = (Map<string,object>) obj;
            count++;
            if(sb.get(selectedPickListValue) !=null && mapCount.containsKey(string.valueof(sb.get(selectedPickListValue)))){
                mapCount.put(string.valueof(sb.get(selectedPickListValue)),mapCount.get(string.valueof(sb.get(selectedPickListValue)))+count);
            }else if(sb.get(selectedPickListValue) !=null){
                mapCount.put(string.valueof(sb.get(selectedPickListValue)),count);
            }else{
                if(sb.get(selectedPickListValue) ==null && mapCount.containsKey('NONE')){
                    mapCount.put('NONE',mapCount.get('NONE')+count);
                }else if(sb.get(selectedPickListValue) ==null){
                    mapCount.put('NONE',count);
                }
            }
            count=0;
        }
        system.debug('serialize:::'+JSON.serialize(mapCount));
        for(string str : mapCount.keySet()){
            jsonData=jsonData+'{ "Label":"'+str+'", "Data":'+mapCount.get(str)+' },';
        }
        if(jsonData!=''){
            jsonData = jsonData.removeEnd(',');
            jsonData='['+jsonData+']';
        }
        return jsonData;
    }
    public List<PickListWrapper> returnAllPickListField(){
        Map<string,schema.SobjectField> mapFields = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
        for(string sch : mapFields.keySet()){
            schema.describeFieldResult desRes = mapFields.get(sch).getDescribe();
            if(string.valueof(desRes.getType()).equalsIgnoreCase('PICKLIST')){
                lstOfPickListValues.add(new PickListWrapper(desRes.getLabel(),desRes.getName()));
            }
        }
        return lstOfPickListValues;
    } 
    global class PickListWrapper{
        public String Label{get;set;}
        public String Value{get;set;}
        
        public PickListWrapper(String Label, String Value){
            this.Label = Label;
            this.Value = Value;
        }
    }
    
    public void registerUser() {
        String exceptionText='';
        try {
            
            String userId = UserInfo.getUserId();
            User u = [Select MobilePhone, Id from User Where Id=:userId];
            //mobilePhone = getFormattedSms(mobilePhone); 
           //if (mobilePhone != null && mobilePhone != '') {
           u.MobilePhone = '+918800177340';
           update u;
            // We're updating the email and phone number before verifying. Roll back  
           //  the change in the verify API if it is unsuccessful.
            exceptionText = System.UserManagement.initRegisterVerificationMethod(Auth.VerificationMethod.SMS);
            if(exceptionText!= null && exceptionText!=''){
                //isInit = false;
                //showInitException = true;
            } else {
                 //isInit = false;
                 //isVerify = true;
            }
        } catch (Exception e) {
            system.debug('exceptionText :: '+exceptionText);
            exceptionText = e.getMessage();
            //isInit = false;
            //showInitException = true;
        }
        system.debug('exceptionText :: '+exceptionText);
    }

    public void verifyUser() {
        // Take the user’s input for the code sent to their phone number
        //String exceptionText = System.UserManagement.verifyRegisterVerificationMethod(code, Auth.VerificationMethod.SMS);
        
    }
    global static void test(){
       Map<id, Contact> oldmap =  (Map<Id,Contact>)Trigger.oldMap;
        system.debug('oldmap :: '+oldmap);
    }
}