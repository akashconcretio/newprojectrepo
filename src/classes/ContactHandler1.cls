public class ContactHandler1 implements TDTMInterface{
    
	Public void onBeforeInsert(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        system.debug('ContactHandler1 :: '+'onBeforeInsert');
    }
    
    Public void onAfterInsert(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        system.debug('ContactHandler1 :: '+'onAfterInsert');
    }
    
    Public void onBeforeUpdate(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        system.debug('ContactHandler1 :: '+'onBeforeUpdate');
    }
    
    Public void onAfterUpdate(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        system.debug('ContactHandler1 :: '+'onAfterUpdate');
    }
    Public void onAfterDelete(List<Sobject> newObjectList,List<Sobject> oldObjectList){
        
    }

}