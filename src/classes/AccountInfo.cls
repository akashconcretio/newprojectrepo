public with sharing class AccountInfo{
  @AuraEnabled
  public static list<Account> getAccountRecords(){
      return [select Name,Phone,Website from Account];
  }
  
  @AuraEnabled
  public static list<Account> getAccountIndustryRecords(){
      return [select Name,Industry,Website from Account where Industry != null];
  }
}