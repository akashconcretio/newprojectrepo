public class CSDEMO {
    
  private String baseQuery = 'Select ID, Name FROM Account ORDER BY NAME ASC';
  public String AccFilterId ;
  private Integer pageSize = 10;

  public CSDEMO(){}

  public ApexPages.StandardSetController AccSetController {
        get{
            if(AccSetController == null){
                AccSetController = new ApexPages.StandardSetController(Database.getQueryLocator(baseQuery));
                AccSetController.setPageSize(pageSize);

                // We have to set FilterId after Pagesize, else it will not work
                if(AccFilterId != null)
                {
                  AccSetController.setFilterId(AccFilterId);
                }
            }
            return AccSetController;
        }set;
    }

  public CSDEMO(ApexPages.StandardSetController c) {   }

    public List<Account> getProps()
    {
      return (List<Account>)AccSetController.getRecords();
    }

    //Get all available list view for Account
    public List<SelectOption> getAccountExistingViews(){
        List<SelectOption> d = new List<SelectOption>(); 
        d.add(new SelectOption('none','none'));
        d.addAll(AccSetController.getListViewOptions());
        //d.add(AccSetController.getListViewOptions());
        return d;
    }

    /**
    * Reset List View
    */
    public PageReference resetFilter()
    {
      AccSetController = null;
        AccSetController.setPageNumber(1);
        return null;
    }
}


/*
to get cron jobs
SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType, NextFireTime, State, PreviousFireTime,StartTime,EndTime,CronExpression FROM CronTrigger
*/