/*
    @Author :: Akash Kumar || 14 Nov 2019
    @Description :: This class is created to hit autopilat Api to retrieve All lead's very first landing URL as per lead's Email . 
                    
                    NOTE :: Batch size must not be greater than 100 as we can have 100 callouts in one batch execution . 
*/
global class AP_LeadBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        // retrieve all  leads in start method
        List<String> email =new List<String>{'scottyu@mila-international.com','dhillonm@msu.edu'};
        return Database.getQueryLocator('select id,email,Initial_Landing_Url__c from lead where email IN:email');
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<Lead> lstLead = (List<Lead>) scope;
        
        for(Lead ld : lstLead){
            if(ld.email != null){
                // call this method to make api hit as per lead's email .
                AP_IntegrationService.retrieveLeadFirstLandingUrlInfo(ld);
            }
        }
        // finally update all leads togther  . 
        update lstLead;
    }
    
    global void finish(Database.BatchableContext BC){
        
        
    }
}