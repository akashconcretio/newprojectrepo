/**
* @author Badan Singh Pundeer
* @date 30.05.2018
*
* @group CBM
*
* @description Construct Raw Data For SpenderSelectionConfigurationPage.
*
* @history
* version                           | author                                    | changes
*=============================================================================
* 1.0 (30.05.2018)          | Badan Pundeer                   | initial version
*/
public class CCG_ConstructDataForSpenderSelection {
	public CCG_ConstructDataForSpenderSelection(){
        
    }
     /**
    * @description constructing data for Columns.
    */
    public Static String getAccountContactFieldsForColumns(){
        List<CCG_FilterFieldTabDetailWrpr> lstOfFilterFieldTabWrpr = new List<CCG_FilterFieldTabDetailWrpr>();
        List<String> lstObj = new List<String>{'Account','Contact','Lead'};
        for(string str : lstObj){
            lstOfFilterFieldTabWrpr.add(setFields(str,new Spender_Selektion_Object__c()));
        }
        return JSON.serialize(lstOfFilterFieldTabWrpr);
    }
    
     /**
    * @description this methos is called when page loads and provide Raw content data for FilterFieldsTab.
    * @return returns a serialized wrapper class that contains object Name , unselected fields and selected fields for a object in multi-select list.
    */
    @RemoteAction
    public static String allAboutObjectSpecificFilter(){
        List<CCG_FilterFieldTabDetailWrpr> lstOfFilterFieldTabWrpr = new List<CCG_FilterFieldTabDetailWrpr>();
        Set<String> lstObjeApiNameSelected = new Set<String>();
        
        List<Spender_Selektion_Object__c> selectedObject = [select Name,Account_Field__c,Filter_Fields__c,Object_Name__c from Spender_Selektion_Object__c];
        for(Spender_Selektion_Object__c cbm : selectedObject){
            lstObjeApiNameSelected.add(cbm.Name);
            lstOfFilterFieldTabWrpr.add(setFields(cbm.Name,cbm));
        }
        return getAllObjects(lstOfFilterFieldTabWrpr,lstObjeApiNameSelected);// change request.07 june
    }
    
    public static string getAllObjects(List<CCG_FilterFieldTabDetailWrpr> wrpr,Set<String> lstObjeApiNameSelected){
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        List<ObjectDescription> lstDscr = new List<ObjectDescription>();
        
        for(String obj : gd.keySet() ){
            if(!lstObjeApiNameSelected.contains(obj)){
                lstDscr.add(new ObjectDescription(obj,gd.get(obj).getDescribe().getLabel()));
            }
        }
        return JSON.serialize(new CCG_SpenderSelektionDataWrpr(wrpr,lstDscr));
    }
    
    /**
    * @description this method provides us the object specific selected and non-selected values for multi-select picklist.
    * @CalledFrom  allAboutObjectSpecificFilter().
    * @param obj, Object Name
    * @param lstOfUnselectedFields, list of Unselected Fields for a particular object .
    * @param lstOfSelectedFields, list of Selected Fields for a particular object .
    * @return returns wrapper class CCG_FilterFieldTabDetailWrpr containing object name,label,selected fields and un-selected fields.
    */
    public static CCG_FilterFieldTabDetailWrpr setFields(string obj,Spender_Selektion_Object__c selObj){
        List<CCG_FieldDetailWrapper> lstOfSelectedFields = new List<CCG_FieldDetailWrapper>();
        List<CCG_FieldDetailWrapper> lstOfUnselectedFields = new List<CCG_FieldDetailWrapper>();
        
        SObjectType sobjType = Schema.getGlobalDescribe().get(obj);
        Map<String,Schema.SObjectField> mfields = sobjType.getDescribe().fields.getMap();
        List<String> lstSelectedFields = new List<String>();
        
        if(selObj!=null && selObj.Filter_Fields__c!=null){
            lstSelectedFields.addAll(selObj.Filter_Fields__c.split(','));
        }
        
        for(string str : mfields.keySet()){
            if(mfields!=null && mfields.get(str).getDescribe().isFilterable() && !(String.valueof(mfields.get(str).getDescribe().getType())=='Address')){
                string fApiName = String.valueof(mfields.get(str).getDescribe().getName());
                string fLabel = String.valueof(mfields.get(str).getDescribe().getLabel());
                if(!lstSelectedFields.contains(fApiName)){
                    lstOfUnselectedFields.add(new CCG_FieldDetailWrapper(fApiName,fLabel));
                }else{
                    lstOfSelectedFields.add(new CCG_FieldDetailWrapper(fApiName,fLabel));
                }
            }
        }
        return new CCG_FilterFieldTabDetailWrpr(obj,sobjType.getDescribe().getLabel(),lstOfUnselectedFields,lstOfSelectedFields);
    }
}