public class clsWrapperExample1 {
public list<clsWrapper> lstWrapper{get;set;}
    public clsWrapperExample1()
    {
        lstWrapper = new list<clsWrapper>();
        
        for(Account obj : [select Id,Name,Website,
                            (select Id from Contacts),
                            (select Id from Cases),
                            (select Id from Opportunities)
                            from Account])
        {
            clsWrapper objW = new clsWrapper();
            objW.objAcc = obj;
            if(obj.Opportunities != null)
                objW.intOppCount = obj.Opportunities.size(); 
            
            if(obj.Cases != null)
                objW.intCaseCount = obj.Cases.size();
            
            if(obj.Contacts != null)
                objW.intConCount = obj.Contacts.size();
                
            lstWrapper.add(objW);
        }
    }
    
    public class clsWrapper
    {
        public Account objAcc{get;set;}
        public integer intOppCount{get;set;}
        public integer intConCount{get;set;}
        public integer intCaseCount{get;set;}
    }
}