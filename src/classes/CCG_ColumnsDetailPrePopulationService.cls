/**
* @author Badan Singh Pundeer
* @date 29.05.2018
*
* @group CBM
*
* @description wrapper class to hold the columns tab data as per Rows.
*
* @history
* version                           | author                                    | changes
*=============================================================================
* 1.0 (29.05.2018)          | Badan Pundeer                   | initial version
*/
public class CCG_ColumnsDetailPrePopulationService {
    public string name;
    public string accountField;
    public string contactField;
    public Boolean isFixed;
    public String leadField;
    public CCG_ColumnsDetailPrePopulationService(string name,string accountField,string contactField,Boolean isFixed,String leadField){
        this.name = name;
        this.accountField = accountField;
        this.contactField = contactField;
        this.isFixed = isFixed;
        this.leadField = leadField;
    }
}