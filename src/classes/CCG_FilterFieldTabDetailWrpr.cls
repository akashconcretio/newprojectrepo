/**
* @author Badan Singh Pundeer
* @date 29.05.2018
*
* @group CBM
*
* @description wrapper class that holds fields per object to show in multiselect picklist(selected and non-selected fields).
*              we set No of filter fields available for main page.
*
* @history
* version                           | author                                    | changes
*=============================================================================
* 1.0 (29.05.2018)          | Badan Pundeer                   | initial version
*/

public class CCG_FilterFieldTabDetailWrpr {
	public string obj;
    public string objLabel;
    public List<CCG_FieldDetailWrapper> unSelectedFields;
    public List<CCG_FieldDetailWrapper> selectedFields;
    public CCG_FilterFieldTabDetailWrpr(string obj,string objLabel,List<CCG_FieldDetailWrapper> unSelectedFields,List<CCG_FieldDetailWrapper> selectedFields){
        this.obj = obj;
        this.unSelectedFields = unSelectedFields;
        this.selectedFields = selectedFields;
        this.objLabel = objLabel;
    }
}