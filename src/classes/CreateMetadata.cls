public class CreateMetadata { 
    
    public void createMetadataRecords(){
         // Set up custom metadata to be created in the subscriber org.
        Metadata.CustomMetadata customMetadata =  new Metadata.CustomMetadata();
        customMetadata.fullName = 'My_First_Custom_Metadata.Not_Found_Code';
        customMetadata.label = 'Not_Found_Code';
    
        Metadata.CustomMetadataValue customField = new Metadata.CustomMetadataValue();
        customField.field = 'Status__c';
        customField.value = '4024';
        customField.field = 'text__c';
        customField.value = 'hello';
    
        customMetadata.values.add(customField);
    
        Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
        mdContainer.addMetadata(customMetadata);
    
        // Setup deploy callback, MyDeployCallback implements
        // the Metadata.DeployCallback interface (code for
        // this class not shown in this example)
        CustomMetadataCallback callback = new CustomMetadataCallback();
    
        // Enqueue custom metadata deployment
        // jobId is the deployment ID
        Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
    }
}