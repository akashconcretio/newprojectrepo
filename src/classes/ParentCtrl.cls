public class ParentCtrl {

    @AuraEnabled
    public static String abc(String fName,String lName){
        //throw new BolException(JSON.serialize(new error('Permission','Something Went Wrong')));
        return fName+lName;
    }
    
    @AuraEnabled(cacheable=true)
    public static Account returnAcc(){
        //throw new BolException(JSON.serialize(new error('Permission','Something Went Wrong')));
        return [select id,name,rating,industry from Account limit 1];
        //return fName+lName;
    }
    public class BolException extends Exception{
        
    }
    
    public class error{
        @AuraEnabled public String errorType;
        @AuraEnabled public String errorMessage;
        public error(String errorType,String errorMessage){
            this.errorType = errorType;
            this.errorMessage = errorMessage;
        }
    }
}