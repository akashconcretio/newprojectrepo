public class TWTasksWrapper{
	public String STATUS;	//OK
	public cls_projects[] projects{get;set;}
	public cls_tasklists[] tasklists{get;set;}
	public cls_todo_items[] todo_items{get;set;}
	public cls_people[] people{get;set;}
	
	public class cls_projects {
		public cls_company company;
		public boolean replyByEmailEnabled;
		public boolean starred;
		public String name{get;set;}	//Timesheet Management
		public boolean privacyEnabled;
		public boolean show_announcement;
		public String description;	//Test Project for integration
		public boolean harvest_timers_enabled;
		public String subStatus;	//current
		public boolean isProjectAdmin;
		public String status;	//active
		public String defaultPrivacy;	//open
		public String created_on;	//2016_05_05T07:24:18Z
		public boolean filesAutoNewVersion;
		public cls_category category;
		public String start_page;	//projectoverview
		public cls_tags[] tags;
		public String startDate;	//
		public String logo;	//
		public boolean notifyeveryone;
		public String id{get;set;}	//242649
		public String last_changed_on;	//2016_05_09T07:59:17Z
		public cls_defaults defaults;
		public String endDate;	//
	}
	class cls_company {
		public String name;	//Concretio
		public String is_owner;	//1
		public String id;	//93188
	}
	class cls_category {
		public String name;	//
		public String id;	//
	}
	class cls_tags {
	}
	class cls_defaults {
		public String privacy;	//
	}
	public static TWTasksWrapper parse(String json){
		return (TWTasksWrapper) System.JSON.deserialize(json, TWTasksWrapper.class);
	}
	
	public class TWProjectTasksList{
	public String STATUS;	//OK
	
	}
	public class cls_tasklists {
		public String name{get;set;}	//General tasks
		public boolean pinned;
		public String milestone_id;	//
		public String description;	//
		public Integer uncompleted_count;	//1
		public String id{get;set;}	//785137
		public boolean complete;
		public boolean privateList;
		public Integer position;	//4000
		public String status;	//new
		public String projectId;	//242649
		public String projectName;	//Timesheet Management
		public cls_DLM DLM;
	}
	class cls_DLM {
	}
	public static TWTasksWrapper parseTasksList(String json){
		return (TWTasksWrapper) System.JSON.deserialize(json, TWTasksWrapper.class);
	}
	
	public class TWTasksInTasksList{
	public String STATUS;	//OK
	
    }
    public class cls_todo_items {
    	public Integer id{get;set;}	//6608212
    	public boolean canComplete;
    	public Integer comments_count;	//0
    	public String description;	//
    	public boolean has_reminders;
    	public boolean has_unread_comments;
    	public Integer privateList;	//0
    	public String content{get;set;}	//Teamwork basic integration with Salesforce
    	public Integer order;	//2000
    	public Integer project_id;	//242649
    	public String project_name;	//Timesheet Management
    	public Integer todo_list_id;	//785137
    	public String todo_list_name;	//General tasks
    	public boolean tasklist_private;
    	public String status;	//new
    	public String company_name;	//Concretio
    	public Integer company_id;	//93188
    	public Integer creator_id;	//197209
    	public String creator_firstname;	//Badan
    	public String creator_lastname;	//Singh Pundeer
    	public boolean completed;
    	public String start_date;	//20160505
    	public String due_date_base;	//20160505
    	public String due_date;	//20160505
    	public String created_on;	//2016_05_05T07:37:56Z
    	public String last_changed_on;	//2016_05_06T10:09:19Z
    	public Integer position;	//2000
    	public Integer estimated_minutes;	//0
    	public String priority;	//
    	public Integer progress;	//0
    	public boolean harvest_enabled;
    	public String parentTaskId;	//
    	public String lockdownId;	//
    	public String tasklist_lockdownId;	//
    	public Integer has_dependencies;	//0
    	public Integer has_predecessors;	//0
    	public boolean hasTickets;
    	public String timeIsLogged;	//1
    	public Integer attachments_count;	//0
    	public String responsible_party_ids;	//197210
    	public String responsible_party_id;	//197210
    	public String responsible_party_names;	//Akash S.
    	public String responsible_party_type;	//Person
    	public String responsible_party_firstname;	//Akash
    	public String responsible_party_lastname;	//Singh
    	public String responsible_party_summary;	//Akash S.
    	public cls_predecessors[] predecessors;
    	public boolean canEdit;
    	public boolean viewEstimatedTime;
    	public String creator_avatar_url;	//
    	public boolean canLogTime;
    	public boolean userFollowingComments;
    	public boolean userFollowingChanges;
    	public Integer DLM;	//1467617527957
    }
    public class cls_predecessors {
    }
    public static TWTasksWrapper parseTasksINTasksList(String json){
    	return (TWTasksWrapper) System.JSON.deserialize(json, TWTasksWrapper.class);
    }
    
    
    public class CompanyAllUsers{
    	
    	public String STATUS;	//OK
    }
	public class cls_people {
		public boolean administrator;
		public String address_city;	//
		public String pid;	//
		public boolean site_owner;
		public String twitter;	//
		public String phone_number_home;	//
		public String last_name;	//Gupta
		public String email_address;	//abhinav@concret.io
		public cls_tags[] tags;
		public String userUUID;	//
		public String user_name;	//abhinav@concret.io
		public String company_name;	//Concretio
		public String id{get;set;}	//198035
		public String last_changed_on;	//2016_05_09T07:59:12Z
		public String phone_number_office;	//
		public boolean deleted;
		public String address_country;	//
		public String address_state;	//
		public String notes;	//
		public String phone_number_mobile;	//
		public cls_phone_number_mobile_parts phone_number_mobile_parts;
		public String first_name;	//Abhinav
		public String user_type;	//account
		public cls_permissions permissions;
		public String im_service;	//
		public String user_invited_status;	//COMPLETE
		public String login_count;	//1
		public String im_handle;	//
		public String address_line_2;	//
		public String openId;	//
		public String address_line_1;	//
		public String created_at;	//2016_05_09T07:59:12Z
		public String textFormat;	//HTML
		public String phone_number_office_ext;	//
		public String company_id;	//93188
		public boolean has_access_to_new_projects;
		public String address_zip;	//
		public String user_invited_date;	//2016_05_09T07:59:12Z
		public String phone_number_fax;	//
		public String avatar_url;	//https://concretio1.teamwork.com/images/noPhoto2.png
		public String user_invited;	//2
		public boolean in_owner_company;
		public String last_login;	//2016_05_09T08:00:06Z
		public String email_alt_1;	//
		public String email_alt_2;	//
		public String companyId;	//93188
		public String email_alt_3;	//
		public String title;	//
	}
// 	class cls_tags {
// 	}
	class cls_phone_number_mobile_parts {
		public String phone;	//
		public String prefix;	//
		public String countryCode;	//
	}
	class cls_permissions {
		public boolean can_manage_people;
		public boolean can_add_projects;
	}
	public static TWTasksWrapper parseAllUsers(String json){
		return (TWTasksWrapper) System.JSON.deserialize(json, TWTasksWrapper.class);
	}

}