public class ProjectCalloutService {
    //Complete the implementation for business and callout logic
    // public static list<Id> lstOfOpprnt;
    
    @InvocableMethod
    public static List<string> getIdFromProcess(List<getInput> lstInp){
        List<Id> lstIds = new list<Id>{lstInp[0].oppId};
        postOpportunityToPMS(lstIds);
        return null;
    }
    
    
	public static void postOpportunityToPMS(list<Id> lstOfOpprnt){
        ID jobID = System.enqueueJob(new ProjectCalloutService.QueueablePMSCall(lstOfOpprnt));
        // postToPMS();
    }
    
    // @future
    // public static void postToPMS(){
    //         Http http = new Http();
    //         HttpResponse res = new HttpResponse();
    //         HttpRequest req = new HttpRequest();
    //         req.setEndpoint('Callout:ProjectService');
    //         req.setMethod('POST');
    //         req.setHeader('Accept','application/json');
    //         req.setBody(createBody(lstOfOpp));
    //         req.setHeader('token',cusDet.Token__c);
    //         req.setHeader('content-type','application/json');
    //         res = http.send(req);
            
    //         system.debug('res :  '+res.getBody());
    // }
    public class QueueablePMSCall implements System.Queueable,Database.AllowsCallouts {
        public ServiceTokens__c cusDet =  ServiceTokens__c.getInstance('ProjectServiceToken');
        public list<Id> lstOfOpp;
        public QueueablePMSCall(List<Id>  lstOpps){
            lstOfOpp= lstOpps;
        }
         public void execute(QueueableContext context) {
            //   Account a = new Account(Name='Acme',Phone='(415) 555-1212');
            //   insert a; 
            Http http = new Http();
            HttpResponse res = new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:ProjectService');
            req.setMethod('POST');
            req.setHeader('Accept','application/json');
            
            req.setBody(createBody(lstOfOpp));
            req.setHeader('token',cusDet.Token__c);
            req.setHeader('content-type','application/json');
            req.setTimeout(120000);
            res = http.send(req);
            
            system.debug('lstOfOpp :  '+createBody(lstOfOpp));
            system.debug('res :  '+res.getBody());
            system.debug('res11 :  '+res.getStatusCode());
            system.debug('cusDet :  '+cusDet.Token__c);
            
         }
    }
    
    public static string createBody(list<Id> lstOfOpps){
        List<opportunity> opp = [select id,Name,Account.Name,CloseDate,Amount from opportunity where Id =:lstOfOpps[0]];
        string str = '{"opportunityId":"'+opp[0].Id+'","opportunityName":"'+opp[0].Name+'","accountName":"'+opp[0].Account.Name+'","closeDate":"'+opp[0].closeDate+'","amount":'+opp[0].amount+'}';
        return str;
    }
    
    public class getInput{
        @InvocableVariable
        public Id oppId;
    }
}