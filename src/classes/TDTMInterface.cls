public Interface TDTMInterface {
	void onBeforeInsert(List<Sobject> newObjectList,List<Sobject> oldObjectList);
    void onAfterInsert(List<Sobject> newObjectList,List<Sobject> oldObjectList);
    void onBeforeUpdate(List<Sobject> newObjectList,List<Sobject> oldObjectList);
    void onAfterUpdate(List<Sobject> newObjectList,List<Sobject> oldObjectList);
}