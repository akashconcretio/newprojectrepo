global class flowChat implements Process.Plugin { 

// The main method to be implemented. The Flow calls this at runtime.
global Process.PluginResult invoke(Process.PluginRequest request) { 
        // Get the subject of the Chatter post from the flow
        String subject = (String) request.inputParameters.get('subject');
        
        
        // return to Flow
        Map<String,Object> result = new Map<String,Object>(); 
    	result.put('Name','Akash');
        return new Process.PluginResult(result); 
    } 

    // Returns the describe information for the interface
    global Process.PluginDescribeResult describe() { 
        Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
        //result.setDescription('this plugin gets the name of a user');
        //result.setTag('userinfo');
        result.inputParameters = new List<Process.PluginDescribeResult.InputParameter>{ 
            new Process.PluginDescribeResult.InputParameter('FullName', 
                                                            Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('DOB', 
                                                                Process.PluginDescribeResult.ParameterType.DATE, true)
                }; 
        return result; 
    }
}