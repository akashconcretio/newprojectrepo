public class SobjectsAndFieldsPermissionCheckingUtil{
    public static Boolean checkSObjectPermission(String objectApiNAme,String dmlOperation){
        Boolean isAccessible = Schema.getGlobalDescribe().get(objectApiNAme).getDescribe().isAccessible();
        if(dmlOperation.equalsIgnoreCase('delete') && isAccessible){
            return Schema.getGlobalDescribe().get(objectApiNAme).getDescribe().isDeletable();
        }else if(dmlOperation.equalsIgnoreCase('update') && isAccessible){
            return Schema.getGlobalDescribe().get(objectApiNAme).getDescribe().isUpdateable();
        }else if(dmlOperation.equalsIgnoreCase('insert') && isAccessible){
            return Schema.getGlobalDescribe().get(objectApiNAme).getDescribe().isCreateable();
        }
        return null;
    }
    
    public static Boolean checkFieldsPermission(String objectApiNAme,List<String> lstOfFields,String dmlOperation){
        Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectApiNAme).getDescribe().fields.getMap();
        Boolean allPermissionSet = true;
        Boolean isAccessible = true;
        for(String str : lstOfFields){
            if(fieldMap.containsKey(str)){
                isAccessible = fieldMap.get(str).getDescribe().isAccessible();
                if(dmlOperation.equalsIgnoreCase('insert') && isAccessible){
                    allPermissionSet = fieldMap.get(str).getDescribe().isCreateable();
                }else if(dmlOperation.equalsIgnoreCase('update') && isAccessible){
                    allPermissionSet =  fieldMap.get(str).getDescribe().isUpdateable();
                }
            }
            if(!isAccessible){
                throw new PermissionException('Field '+'\''+str+'\''+ 'is not Accessible');
            }
            if(!allPermissionSet){
                throw new PermissionException('Field '+'\''+str+'\''+ 'is not having '+dmlOperation+' Operation Related Permission.');
            }
        }
        return null;
    }
    
    public static Boolean checkFixFieldPermission(String objectApiNAme,String fieldApiName,String dmlOperation){
        return checkFieldsPermission(objectApiNAme,new List<String>{fieldApiName},dmlOperation);  
    }
}