public class CCG_FilterFieldWrapper {
    public cls_setFilterFields[] setFilterFields;
    public class cls_setFilterFields {
        public String objName;  //Account
        public cls_fields[] fields;
    }
    public class cls_fields {
        public String fieldApiName; //Description
        public String fieldLabel;   //Description
    }
    public static CCG_FilterFieldWrapper parse(String json){
        return (CCG_FilterFieldWrapper) System.JSON.deserialize(json, CCG_FilterFieldWrapper.class);
    }
}