public class CreateContact { 
    public list<Contact> conList{ get; set; }
    public Id accId;
    public Account acc;
    public string rowToBeDeleted{get;set;}
    public Integer count{ get; set; }
    public boolean showhide{get;set;}
    
    public CreateContact (ApexPages.StandardController controller) {
        conList=new list<Contact>();
        count = 0;
        acc = (Account)controller.getRecord();
        accId = acc.Id;
        system.debug('accId-->'+accId);
        showhide=false;
    }
    Public PageReference save(){
        system.debug('conList-->'+conList);
            insert conList;
            PageReference page = new PageReference('/'+ accId);
            page.setRedirect(true);
            return page;
    }
    Public void addAccount()
    {   showhide=true;
      if(count > 0){
           
            for(Integer i =0; i< count; i++ ){
                Contact con = new Contact();
                con.AccountId = accId;
                conList.add(con);           
            }
        } 
    }
    public void addrow()
    {
          Contact con = new Contact();
          con.AccountId = accId;
          conList.add(con);
    }
    public void delrow()
    {
         integer i=conList.size();
         conList.remove(i-1);
    }
    
}