public class CCG_SpenderSelektionDataWrpr {
    public List<CCG_FilterFieldTabDetailWrpr> lstOfFilterFieldTabWrpr;
    public List<ObjectDescription> objeDescr;
    public CCG_SpenderSelektionDataWrpr(List<CCG_FilterFieldTabDetailWrpr> lstOfFilterFieldTabWrpr,List<ObjectDescription> objeDescr){
        this.lstOfFilterFieldTabWrpr = lstOfFilterFieldTabWrpr;
        this.objeDescr = objeDescr;
    }
}