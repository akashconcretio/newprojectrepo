/**
* @author Badan Singh Pundeer
* @date 29.05.2018
*
* @group CBM
*
* @description Wrapper class to hold Columns Input Given By User To save in CS. 
*
* @history
* version                           | author                                    | changes
*=============================================================================
* 1.0 (29.05.2018)          | Badan Pundeer                   | initial version
*/
public class CCG_ColumnDetailWrpr {
    public cls_setColumns[] setColumns;
    public class cls_setColumns {
        public String AccountField; //BillingState
        public String ContactField; //MailingCity
        public String Column;   //ef2e
        public String isFixed;  //ef2e
        public String LeadField;  //ef2e
    }
    public static CCG_ColumnDetailWrpr parse(String json){
        return (CCG_ColumnDetailWrpr) System.JSON.deserialize(json, CCG_ColumnDetailWrpr.class);
    }
}