public class ContactAllActivities {
    public list<contact> lstConAllActivity{get;set;}
    public string conIdwithAllAct{get;set;}
    public ContactAllActivities(){
       conIdwithAllAct = ApexPages.currentPage().getParameters().get('id'); 
       lstConAllActivity = new list<contact>();
       lstConAllActivity = [select id,LastName ,(select Description__c,createdDate from DD_Activities__r) from contact where id =: conIdwithAllAct];
    }
}