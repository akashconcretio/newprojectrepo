public class TeamWorkCreateUpdateTask {
    @Future(callout=true)
    public static void CreateTask(string TaskListId,string TaskToBeUpdated,string taskWhatId){
        HttpRequest req ;
        Http http;
        HTTPResponse res;
        TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
        String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
        req = new HttpRequest(); 
        req.setEndpoint(Configure.Endpoint__c+'tasklists/'+TaskListId+'/tasks.json');
        //system.debug('TList::'+req.getEndpoint());
        req.setMethod('POST');
        req.setHeader('Accept', 'application/json; charset=UTF-8');
        req.setHeader('Content-Type','application/json');  
        req.setHeader('Authorization', 'Basic '+authorizationHeader);
        string taskJsonBody = '{"todo-item":{"content":"'+TaskToBeUpdated+'","notify":false,"description":"Test Task Sub Item","due-date":"20160712","start-date":"20160706","estimated-minutes":"0","private":0,"grant-access-to":"","priority":"low","progress":"20","attachments":{},"pendingFileAttachments":"","responsible-party-id":197210,"predecessors":[{"id": 0,"type":"start"}],"tags":"api,documentation","positionAfterTask":-1}}';
        req.setBody(taskJsonBody);
        req.setTimeout(120000);
        http = new Http();
        res = http.send(req);
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getbody());
        system.debug('resBody:::'+m.get('id'));
        Task tskDetails = [select Task_Id__c,Id,Subject from Task where (subject =:TaskToBeUpdated) AND (whatId =:taskWhatId) limit 1];
        system.debug('resBody:::'+tskDetails);
        tskDetails.Task_Id__c=string.valueof(m.get('id'));
        update tskDetails;
        system.debug('resId:::'+tskDetails.Task_Id__c);
    } 
    @Future(callout=true)
    public static void UpdateTask(string TaskId,string TaskToBeUpdated){
        HttpRequest req ;
        Http http;
        HTTPResponse res;
        TeamWork_Task__c  Configure = TeamWorkConFigForTasks.Configuration;
        String authorizationHeader = EncodingUtil.base64Encode(Blob.valueOf(Configure.Api_Key__c));
        req = new HttpRequest(); 
        req.setEndpoint(Configure.Endpoint__c+'tasks/'+TaskId+'.json');
        //system.debug('TList::'+req.getEndpoint());
        req.setMethod('PUT');
        req.setHeader('Accept', 'application/json; charset=UTF-8');
        req.setHeader('Content-Type','application/json');  
        req.setHeader('Authorization', 'Basic '+authorizationHeader);
        string taskJsonBody = '{"todo-item":{"content":"'+taskToBeUpdated+'","notify":false,"description":"","due-date":"20160720","start-date":"20160706","private":"0","grant-access-to":"","priority":"high","progress":"20","estimated-minutes":"0","responsible-party-id":"197210","attachments":{},"tasklistId":"","pendingFileAttachments":"","tags":"api,documentation"}}';
        req.setBody(taskJsonBody);
        req.setTimeout(120000);
        http = new Http();
        res = http.send(req);
        //system.debug('resBody:::'+res.getBody());
    }
}