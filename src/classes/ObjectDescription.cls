public class ObjectDescription {
	Public String objectApiName;
    Public String objectLabel;
    public List<CCG_FieldDetailWrapper> fieldList;
    public ObjectDescription(String objectApiName,String objectLabel){
        this.objectApiName = objectApiName;
        this.objectLabel = objectLabel;
    }
}