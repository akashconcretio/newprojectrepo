public class AnimalLocator {
    public static String getAnimalNameById(Integer AnimalId){
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/'+AnimalId);
                    req.setMethod('GET');
                    req.setHeader('Accept', 'application/json; charset=UTF-8');
                    req.setHeader('Content-Type','application/json');  
                    //req.setHeader('Authorization', 'Basic '+authorizationHeader);
                    req.setTimeout(120000);
                    
                 Http   http = new Http();
                 HTTPResponse res = http.send(req);
                 animals anm = animals.parse(res.getBody());
                //  SYSTEM.DEBUG('res ::'+res.getBody());
                //  Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                //  Map<String, Object> m1 = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(String.valueof(m.get('animal'))));
        return anm.animal.name;
    }
}