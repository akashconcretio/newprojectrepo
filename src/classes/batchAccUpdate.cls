global class batchAccUpdate implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,Name FROM Account where Payment_Total__c=null';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account> acc) {
         for(Account a : acc){
             if(a.Rating=='Hot'){
                 a.Name = a.Name+' Updated Via Batch'; 
             }           
         }
         update acc;
    }
    global void finish(Database.BatchableContext BC) {
        contact con = new contact();
        con.Lastname = 'created via batch';
        insert con;
    }
}