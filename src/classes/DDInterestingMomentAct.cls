public class DDInterestingMomentAct {
    public list<contact> lstConAllActivity{get;set;}
    public list<contact> lstConFilteredActivity{get;set;}
    public Id conId{get;set;}
    public DDInterestingMomentAct(ApexPages.StandardController controller){
       conId = controller.getId();
       lstConAllActivity = new list<contact>();
       lstConFilteredActivity = new list<contact>();
    }
    @AuraEnabled
    public list<contact> getAllActivity(){
        lstConAllActivity = [select id,LastName ,(select Description__c,createdDate from DD_Activities__r) from contact where id =: conId ];
        return lstConAllActivity;
    }
    @AuraEnabled
    public list<contact> getFilteredActivity(){
        lstConFilteredActivity = [select id,LastName ,(select Description__c,createdDate from DD_Activities__r where Type__c='bookmark') from contact where id =: conId ];
        return lstConFilteredActivity;
    }

}