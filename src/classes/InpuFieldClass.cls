public class InpuFieldClass {
    public string industry {get;set;}
    public string Phone {get;set;}
    public list<Account> lstAcc {get;set;}
    public boolean showhide{get;set;}
    public InpuFieldClass(){
        showhide = false;
    }
    public Pagereference AccIndDetals(){
        showhide = true;
        // AND Phone = :Phone
        system.debug('industry:::'+industry);
       lstAcc = new list<Account>();
       lstAcc = [select Id,Name ,Phone , Industry from Account where Industry = :industry ];
       system.debug('lstAcc:::'+lstAcc);
        return Null;
    }
    public List<SelectOption> getItems() {
    
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Open – Not ','Open – Not Contacted'));
        options.add(new SelectOption('Working – ','Working – Contacted'));
        options.add(new SelectOption('Closed – ','Closed – Converted'));
        options.add(new SelectOption('Closed – Not ','Closed – Not Converted'));
        return options;
    }
}