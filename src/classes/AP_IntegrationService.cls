/*
    @Author :: Akash Kumar || 13 Nov 2019
    @Description :: This class is created to hit autopilat Api to retrieve lead's very first landing URL as per lead's Email . 
*/
 
public class AP_IntegrationService{ 
    
    /*
        @Description :: This method will generate API request and set autopilot configurations from custom setting . 
        
        @param :: urlPath       :: It is lead's email address . 
                  leadId        :: leadId where we need to update landing URL .
    */
    public static void retrieveLeadFirstLandingUrlInfo(Lead lead){
        // here we have generated request.
        HttpRequest req = generateRequestToHitAutoPilotAPI(lead.email);
        
        // return in case no configuration is available in CS . 
        if(req.getEndpoint() == null){
            return;
        }
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        // calling this method to parse response .
        parseResponse(res,lead);
    }
    
    
    /*
        @Description :: This method will generate API request and set autopilot configurations from custom setting . 
        
        @param :: urlPath       :: It is lead's email address . 
    */
    
    public static HttpRequest generateRequestToHitAutoPilotAPI(String urlPath){
        
        // getting Autopilot configuration from custom setting . 
        Autopilot_Configuration__c apConfig = Autopilot_Configuration__c.getInstance('default');
        
        // retrun empty request if there is no configuration in custom setting . 
        if(apConfig == null){
            return new HttpRequest();
        }
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(apConfig.EndPoint__c+urlPath);
        req.setMethod('GET');
        
        req.setHeader('autopilotapikey', apConfig.Api_Key__c);
        return req;
    }
    
    /*
        @Description :: This method is for paring aotopilot api response .  
        
        @param :: res       :: response received from API hit
                  leadId    :: leadId where we need to update landing URL .
    */
    public static void parseResponse(HTTPResponse res,Lead lead){ 
        // getting Api response body having landing URL info
        String response = res.getBody();
        
        // paring main response to Map .
        Map<String, Object> parseMainResponseMap = (Map<String, Object>)JSON.deserializeUntyped(response);
        
        // validating response
        if(!validateResponse(parseMainResponseMap,lead)){
            return;
        }
        
        // paring page visits related response to Map . response is not in complete JSON format to parse directly . So we had to make changes accordingly to make 
        // it complete JSON . 
        Map<String, Object> parseVisitUrlResponseMap = (Map<String, Object>)JSON.deserializeUntyped(String.valueOf(parseMainResponseMap.get('anywhere_page_visits')).replace('//', '"').replace(',', '",').replace('}','"}').replace('=', '":"'));
        
        // here we are getting the very first landing page If Contact on autopilot has more than 1 visiting page . we require the 1st one only .
        String landingFirstUrl = new List<String>(parseVisitUrlResponseMap.keyset())[parseVisitUrlResponseMap.keyset().size()-1];
        
        //creating lead instance with landing uRL and calling the method to update lead
        lead.Initial_Landing_Url__c = landingFirstUrl;
        
        // no need to update if being called in the context of batch .But update If being called from Process Builder .
        if(!system.isBatch()){
            updateLeadWithLandingUrl(lead);
        }
    }
    
    /*
        @Description :: Updating Lead with first landing URL .
        @param       :: leadId   :: leadId where we need to update landing URL .
    */
    public static void updateLeadWithLandingUrl(Lead ld){
        try{
            update ld; 
        }catch(exception ee){
            system.debug('error catched  :: '+ee.getMessage());
        }
    }
    
    /*
        @Description :: Validating response 
        @param :: parseMainResponseMap :: response parsed in map
                  leadId               :: leadId where we need to update landing URL .
    
    */
    public static Boolean validateResponse(Map<String, Object> parseMainResponseMap,Lead lead){
        // cheking whether after parsing map has key  'anywhere_page_visits' or not . If don't then do nothing and return . 
        if(!parseMainResponseMap.containskey('anywhere_page_visits')){
            lead.Initial_Landing_Url__c = null;
            updateLeadWithLandingUrl(lead);
            return false;
        }
        
        // checking after pasring response whether map has error key or not . If yes then there is something worng like contact does't exist in autoPilot , invalid api key . 
        if(parseMainResponseMap.containskey('error')){
            if(String.valueOf(parseMainResponseMap.get('error')).equalsIgnoreCase('Not Found') && parseMainResponseMap.containskey('message') && parseMainResponseMap.get('message') != null && String.valueOf(parseMainResponseMap.get('message')).equalsIgnoreCase('Contact could not be found.')){
                lead.Initial_Landing_Url__c = null;
                updateLeadWithLandingUrl(lead);
            }
            // checking whether Api Key Is valid or not
            else if(String.valueOf(parseMainResponseMap.get('error')).equalsIgnoreCase('Unauthorized') && parseMainResponseMap.containskey('message') && parseMainResponseMap.get('message') != null && String.valueOf(parseMainResponseMap.get('message')).equalsIgnoreCase('autopilotapikey not valid.')){
                 
            }
            return false;
        }
        // checking whether EndPoint is valid or not
        else if(parseMainResponseMap.containskey('code') && String.valueOf(parseMainResponseMap.get('code')) == 'ResourceNotFound'){// 
            return false;
        }
        return true;
    }
    
}