public class Product{
	public cls_Equipment[] Equipment;
	public class cls_Equipment {
		public String id;	//55d66226726b611100aaf741
		public boolean replacement;
		public Integer quantity;	//5
		public String name;	//Generator 1000 kW
		public Integer maintenanceperiod;	//365
		public Integer lifespan;	//120
		public Integer cost;	//5000
		public String sku;	//100003
	}
	public static Product parse(String json){
		return (Product) System.JSON.deserialize(json, Product.class);
	}
}